/*
* This project was founded as a
* team project at CTU in Prag
* by team RoboSnek.
*/
#ifndef __ROBOSNEKCONTROLLER_H__
#define __ROBOSNEKCONTROLLER_H__

#include "pico/stdlib.h"
#include "hardware/flash.h"
#include "hardware/irq.h"

#define CONTROL_UNIT_POSITION 0x00
#define CONTROL_UNIT_VELOCITY 0x01
#define CONTROL_UNIT_FORCE 0x02

typedef struct {
  float P;
  float I;
  float D;
} PID_VALUES;

typedef struct {
  float *data;
  int iterator;
  float sum;
  int size;
} circularBuffer;

class RoboSnekController {

private:
  float *feedback;
  float *controls;
  float *states;
  float *max_controls;
  float *max_states;
  const uint8_t *outPins;
  const uint8_t *inPins;
  uint16_t adcZeroOffset;
  float *feedbackOffset;
  float *rawAngle;
  bool overCurrent;
  // Methods
  uint8_t updateFeedback(uint timeout_us);
  void writeControls();
  bool updateFlash();
  bool updateFromFlash();

public:
  float *setpoint;
  uint64_t controlRate; //Hz
  uint8_t controlledUnit;
  bool controllerOn;
  bool controllerOnRequest;
  bool controllerOffRequest;
  circularBuffer *MotorsCurrent;

  bool calibrationRequest;

  PID_VALUES *positionPID;
  PID_VALUES *velocityPID;
  PID_VALUES *torquePID;

  bool calibrate();
  bool setPID(uint8_t *data);
  float* getLastFeedback();
  float* getStates();
  float* getControls();
  bool controllerStep();
  void enableOutput(bool enable);
  uint8_t checkFeedback();
  RoboSnekController(uint8_t *out_pins, uint8_t *in_pins, uint64_t controlRate);
};

#endif
