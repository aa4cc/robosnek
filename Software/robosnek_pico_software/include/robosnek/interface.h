/*
* This project was founded as a
* team project at CTU in Prag
* by team RoboSnek.
*/
#ifndef __ROBOSNEKINTERFACE_H__
#define __ROBOSNEKINTERFACE_H__

#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "robosnek/controller.h"

#include "robosnek/interface_msgs.h"

enum COMSTATE {
  //  WAIT_FOR_INIT,
  //  WAIT_FOR_ADDRESS_LENGTH,
  //  WAIT_FOR_ADDRESS,
    WAIT_FOR_START,
    WAIT_FOR_LENGTH,
    WAIT_FOR_DATA,
};

class RoboSnekInterface {

private:
  uint8_t comState;
  uint8_t *dataToSend;

  uint8_t myAddress;
  uint8_t header[2];
  uint8_t bytesSent;

  uart_inst_t* uartToMaster;
  uart_inst_t* uartFromMaster;

  RoboSnekController* Controller;

  void react();
public:
  uint8_t *dataForMe;
  uint8_t checkState(); // check usart flags, retransmit stuff, react

  RoboSnekInterface(uint8_t* pinsUart0, uint8_t* pinsUart1, RoboSnekController* controller, uint baudRate, uint databits, uart_parity_t parity, uint stopBits);
};



#endif
