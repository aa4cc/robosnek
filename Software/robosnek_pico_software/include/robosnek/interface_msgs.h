#ifndef __ROBOSNEKMSGS_H__
#define __ROBOSNEKMSGS_H__


// Address definitions and broadcasts ---------
#define MSG_MASTER_ADDRESS 0x00


#define MSG_ADDR_BROADCAST 0xff

// Command definitions ------------------------
#define MSG_COMMAND_GET_ANGLES 0x00
#define MSG_COMMAND_GET_CURRENT 0x01

#define MSG_COMMAND_SET_POSITION 0x02
#define MSG_COMMAND_SET_VELOCITY 0x03
#define MSG_COMMAND_SET_FORCE 0x04

#define MSG_COMMAND_SET_PID 0x0A
#define MSG_COMMAND_SET_ANGLE_ZERO_POINT 0x0B

#define MSG_COMMAND_ERROR_OCCURED 0xfd
#define MSG_COMMAND_KILL 0xfe
#define MSG_COMMAND_SET_ADDRESS 0xff

// Errors definitions -------------------------
#define MSG_ERROR_FEEDBACK_ERROR 0x00
#define MSG_ERROR_UNSUPORTED_COMMAND 0x01


#define MSG_ERROR_DEVICE_UNINIT 0xff

#endif
