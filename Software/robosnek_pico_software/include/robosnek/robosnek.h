/*
* This project was founded as a
* team project at CTU in Prag
* by team RoboSnek.
*/

#include "robosnek/controller.h"



class RoboSnek {

private:

  double *joints;                           // Joint coordinates if any
  double *acceleration;                     // Data from gyro/acceromerter
  double *oddometry;                        // Wheel oddometry if any
  double *forces;                           // Forces measuder on bend_module
  RoboSnekController *Controllers;          // Array of controllers for given hw

public:

  RoboSnek();
  ~RoboSnek();
  void getCurretJoints(float* joints);
  void getProperty();
  void getAcceleration();
};


void usart_rx_cb();
void usart_tx_cb();
