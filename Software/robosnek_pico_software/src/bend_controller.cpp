//This file will contain controller implementation and robosnek object

//#define __I2C_DEFINED_ELSEWHERE__
//#define __ADC_DEFINED_ELSEWHERE__

#include "robosnek/controller.h"
#include "robosnek/robosnek.h"

#include "hardware/pwm.h"
#include "hardware/i2c.h"
#include "hardware/adc.h"
#include "hardware/uart.h"

#include <stdio.h>



#define PWM_CLK_FRAC 25,10 //AS IS! Format: denumerator,multiplyer
#define PWM_WRAP 20000
#define USED_I2C i2c1
#define I2C_CLK 800000
#define ENC_ADDR 0b0110110
const uint8_t ENC_ANGLE_REG = 0x0E;
const uint8_t ENC_STATUS_REG = 0x0B;
#define ENC_MASK_MD 0b00100000
#define ENC_MASK_ML 0b00010000
#define ENC_MASK_MH 0b00001000

#define ADC_REF 3.3
#define SHUNT_RESISTANCE 0.12

#define CONTROLS_MAX 1000.0 //I GUESS?
#define ABS_MAX_COTROLS 1000.0
#define POSITION_MIN 10.0
#define POSITION_MAX 170.0

#define MOTOR_CURRENT_UPPER 0.700 // [A]
#define MOTOR_CURRENT_LOWER 0.400 // [A]

#define FEEDBACK_ERR_VAL -1.0

#define FLASH_DATA_VALID 0xAA
#define FLASH_TARGET_OFFSET (256 * 1024)

// RoboSnekController definition
RoboSnekController::RoboSnekController(uint8_t *outpins, uint8_t *inpins, uint64_t controlrate){
    outPins = outpins;
    inPins = inpins;
    controlRate = controlrate;
    // init PWM
    gpio_set_function(outPins[0], GPIO_FUNC_PWM);
    gpio_set_function(outPins[1], GPIO_FUNC_PWM);
    uint slice_num = pwm_gpio_to_slice_num(outPins[0]);
    pwm_set_clkdiv_int_frac(slice_num, PWM_CLK_FRAC);
    pwm_set_wrap(slice_num, PWM_WRAP);
    slice_num = pwm_gpio_to_slice_num(outPins[1]);
    pwm_set_clkdiv_int_frac(slice_num, PWM_CLK_FRAC);
    pwm_set_wrap(slice_num, PWM_WRAP);

    // Init i2c
    #ifndef __I2C_DEFINED_ELSEWHERE__
    i2c_init(i2c0, I2C_CLK);
    gpio_set_function(inPins[0], GPIO_FUNC_I2C);
    gpio_set_function(inPins[1], GPIO_FUNC_I2C);
    i2c_init(i2c1, I2C_CLK);
    gpio_set_function(inPins[2], GPIO_FUNC_I2C);
    gpio_set_function(inPins[3], GPIO_FUNC_I2C);
    #endif
    //init ADC for current measurement
    #ifndef __ADC_DEFINED_ELSEWHERE__
    adc_init();
    #endif
    adc_gpio_init(inPins[4]);
    //ASSUMING NO CURRENT DURING CALIBRATION
    uint adc_reading = 0;
    adc_select_input(inPins[4] - 26);
    for (uint8_t i = 0; i<10;i++){
      adc_reading += adc_read();
      sleep_us(10);
    }
    adc_reading/=10;
    adcZeroOffset = adc_reading;

    // Init variables
    positionPID = new PID_VALUES;
    positionPID->P = 20;
    positionPID->I = 0.01;
    positionPID->D = 1;
    velocityPID = new PID_VALUES;
    velocityPID->P = 10;
    velocityPID->I = 0.01;
    velocityPID->D = 1;
    torquePID = new PID_VALUES;
    torquePID->P = 100;
    torquePID->I = 0.0;
    torquePID->D = 40;

    feedback = new float[2](); // position1, position2
    //feedback[0] = 0;
    //feedback[1] = 0;
    //feedback[2] = 0;
    feedbackOffset = new float[2](); // position1, position2
    feedbackOffset[0] = 0;
    feedbackOffset[1] = 0;
    rawAngle = new float[2]();
    //feedbackOffset[2] = adcZeroOffset;
    controls = new float[1]();
    //controls[0] = 0;
    setpoint = new float[1]();
    //setpoint[0] = 0;
    //states = new float[2](); // position, current
    //states[0] = 0;
    //states[1] = 0;

    max_controls = new float[2];
    max_controls[0] = -CONTROLS_MAX;
    max_controls[1] = CONTROLS_MAX;
    max_states = new float[2];
    max_states[0] = POSITION_MIN;
    max_states[1] = POSITION_MAX;
    //max_states[2] = -CURRENT_MAX;
    //max_states[3] =  CURRENT_MAX;
    MotorsCurrent = new circularBuffer;
    MotorsCurrent->size = (int)(controlRate/2); // for 0.5 s of data
    MotorsCurrent->data = new float[MotorsCurrent->size]();
    MotorsCurrent->iterator = 0;
    MotorsCurrent->sum = 0;
    overCurrent = false;
    //MotorsCurrent[0] = 0;

    RoboSnekController::updateFromFlash();

    controllerOn = false;
    controllerOnRequest = false;
    controllerOffRequest = false;
    controlledUnit = CONTROL_UNIT_POSITION;

    calibrationRequest = false;

    RoboSnekController::enableOutput(true);
    RoboSnekController::writeControls();
}

void RoboSnekController::enableOutput(bool enable){
  uint slice_num = pwm_gpio_to_slice_num(outPins[0]);
  pwm_set_enabled(slice_num, enable);
  slice_num = pwm_gpio_to_slice_num(outPins[1]);
  pwm_set_enabled(slice_num, enable);
}

uint8_t RoboSnekController::updateFeedback(uint timeout_us){ //TODO MEASURE CURRENTS
  uint8_t data[2];
  uint16_t angle;
  int state;
  //printf("about to Measure!\n");
  //timeout_us = 1000;
  //printf("%i \n", timeout_us);
  i2c_inst_t *I2Cx = i2c0;
  uint8_t ret_state = 0x00;
  for(int i = 0; i < 2; i++){
    state = i2c_write_timeout_us(I2Cx, ENC_ADDR, &ENC_ANGLE_REG, 1, true, timeout_us);
    // TODO: Do something with the state
    if(state != 1) {
      feedback[i] = FEEDBACK_ERR_VAL;
      ret_state |= 0x01 << (i*2);
      I2Cx = i2c1;
      continue;
    }
    state = i2c_read_timeout_us(I2Cx, ENC_ADDR, data, 2, false, timeout_us);
    // TODO: Do something with the state

    if(state != 2) {
      feedback[i] = FEEDBACK_ERR_VAL;
      ret_state |= 0x02 << (i*2);
      I2Cx = i2c1;
      continue;
    }
    angle = (data[0]<<8)|data[1];
    rawAngle[i] = ((float)angle)/4095.0 * 360.0;
    feedback[i] =  rawAngle[i]- feedbackOffset[i];
    if(i == 1) feedback[i] *= -1;
    if (feedback[i] < 0) feedback[i] = 360.0+feedback[i];
    // TODO convert the shit to -180 to 180?
    I2Cx = i2c1;
  }

  // Sense current
  adc_select_input(inPins[4] - 26);
  uint adc_reading = 0;// - adcZeroOffset;
  for (uint8_t i = 0; i<10;i++){
    adc_reading += adc_read();
    sleep_us(1);
  }
  adc_reading/=10;
  if (adc_reading < adcZeroOffset) adc_reading = 0; //cap on 0 to prevent underflow
  else adc_reading-=adcZeroOffset;
  //printf("I read %i, %i\n",adc_reading, adcZeroOffset);
  //feedback[2] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;
  //MotorsCurrent[0] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;
  // add to circularBuffer
  MotorsCurrent->sum -= MotorsCurrent->data[MotorsCurrent->iterator]; // substract previous data in buffer
  MotorsCurrent->data[MotorsCurrent->iterator] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;
  MotorsCurrent->sum += MotorsCurrent->data[MotorsCurrent->iterator];
  MotorsCurrent->iterator = (((++MotorsCurrent->iterator) == MotorsCurrent->size)? 0 : MotorsCurrent->iterator);
  if (!overCurrent && MotorsCurrent->sum/MotorsCurrent->size > MOTOR_CURRENT_UPPER) {
    overCurrent = true;
  }
  else if(overCurrent && MotorsCurrent->sum/MotorsCurrent->size < MOTOR_CURRENT_LOWER){
    overCurrent = false;
  }
  //printf("about to control!\n");
  return ret_state;
}

void RoboSnekController::writeControls(){
  if (controls[0] < max_controls[0]){
    controls[0] = max_controls[0];
  }
  if (controls[0] > max_controls[1]){
    controls[0] = max_controls[1];
  }
    if (controls[0]>=0){
    uint16_t toSet = (uint16_t)(controls[0]/ABS_MAX_COTROLS*PWM_WRAP);
    //printf("about to write %i!\n", toSet);
    pwm_set_gpio_level(outPins[0], toSet);
    pwm_set_gpio_level(outPins[1], 0);
  } else {
    uint16_t toSet = (uint16_t)((-1*controls[0])/ABS_MAX_COTROLS*PWM_WRAP);
    //printf("about to write %i!\n", toSet);
    pwm_set_gpio_level(outPins[0], 0);
    pwm_set_gpio_level(outPins[1], toSet);
  }
}

uint8_t RoboSnekController::checkFeedback(){
  uint8_t state;
  i2c_write_blocking(USED_I2C, ENC_ADDR, &ENC_STATUS_REG, 1, true);
  i2c_read_blocking(USED_I2C, ENC_ADDR, &state, 1, false);
  if(!(state&ENC_MASK_MD)){
    return 0x01;
  }
  if(!(state&ENC_MASK_MH)){
    return 0x02;
  }
  if(!(state&ENC_MASK_MH)){
    return 0x03;
  }
  return 0x00;
}

float* RoboSnekController::getLastFeedback(){
    return feedback;
}
float* RoboSnekController::getStates(){
  return states;
}
float* RoboSnekController::getControls(){
  return controls;
}

float floatFromData(uint8_t *data) {
  union {
    float f;
    uint8_t b[4];
  } floatFromBytes;
  floatFromBytes.b[0] = data[0];
  floatFromBytes.b[1] = data[1];
  floatFromBytes.b[2] = data[2];
  floatFromBytes.b[3] = data[3];
  return floatFromBytes.f;
}

bool RoboSnekController::updateFromFlash(){ //in order: feedbackOffset, positionPID, velocityPID, torquePID
  //TODO: implement loading data from flash
  uint8_t *dataLoaded = (uint8_t *) (XIP_BASE + FLASH_TARGET_OFFSET);
  if (dataLoaded[0] == FLASH_DATA_VALID) {
    dataLoaded++;
    feedbackOffset[0] = floatFromData(dataLoaded);
    dataLoaded += 4;
    feedbackOffset[1] = floatFromData(dataLoaded);
    dataLoaded += 4;
    positionPID->P = floatFromData(dataLoaded);
    dataLoaded += 4;
    positionPID->I = floatFromData(dataLoaded);
    dataLoaded += 4;
    positionPID->D = floatFromData(dataLoaded);
    dataLoaded += 4;
    velocityPID->P = floatFromData(dataLoaded);
    dataLoaded += 4;
    velocityPID->I = floatFromData(dataLoaded);
    dataLoaded += 4;
    velocityPID->D = floatFromData(dataLoaded);
    dataLoaded += 4;
    torquePID->P = floatFromData(dataLoaded);
    dataLoaded += 4;
    torquePID->I = floatFromData(dataLoaded);
    dataLoaded += 4;
    torquePID->D = floatFromData(dataLoaded);
    dataLoaded += 4;
    return true;
  } else {
    return false;
  }
}

int addDataFromFloat(float inp, uint8_t *data, int position){
  union {
    float f;
    uint8_t b[4];
  } floatToBytes;
  floatToBytes.f = inp;
  data[position++] = floatToBytes.b[0];
  data[position++] = floatToBytes.b[1];
  data[position++] = floatToBytes.b[2];
  data[position++] = floatToBytes.b[3];
  return position;
}

bool RoboSnekController::updateFlash(){ // in order: feedbackOffset, positionPID, velocityPID, torquePID
  //TODO: implement storing data into flash
  uint8_t *flash_target_contents = (uint8_t *) (XIP_BASE + FLASH_TARGET_OFFSET);
  uint8_t dataToStore[FLASH_PAGE_SIZE];
  int position = 0;
  dataToStore[position++] = FLASH_DATA_VALID;
  position = addDataFromFloat(feedbackOffset[0], dataToStore, position);
  position = addDataFromFloat(feedbackOffset[1], dataToStore, position);
  position = addDataFromFloat(positionPID->P, dataToStore, position);
  position = addDataFromFloat(positionPID->I, dataToStore, position);
  position = addDataFromFloat(positionPID->D, dataToStore, position);
  position = addDataFromFloat(velocityPID->P, dataToStore, position);
  position = addDataFromFloat(velocityPID->I, dataToStore, position);
  position = addDataFromFloat(velocityPID->D, dataToStore, position);
  position = addDataFromFloat(torquePID->P, dataToStore, position);
  position = addDataFromFloat(torquePID->I, dataToStore, position);
  position = addDataFromFloat(torquePID->D, dataToStore, position);

  flash_range_erase(FLASH_TARGET_OFFSET, FLASH_SECTOR_SIZE);
  flash_range_program(FLASH_TARGET_OFFSET, dataToStore, FLASH_PAGE_SIZE);

  return true;
}

bool RoboSnekController::setPID(uint8_t *data){
  irq_set_enabled(TIMER_IRQ_0, false);
  irq_set_enabled(TIMER_IRQ_1, false);
  irq_set_enabled(TIMER_IRQ_2, false);
  irq_set_enabled(TIMER_IRQ_3, false);
  union {
    float f;
    uint8_t b[4];
  } PBytesToFloat;
  union {
    float f;
    uint8_t b[4];
  } IBytesToFloat;
  union {
    float f;
    uint8_t b[4];
  } DBytesToFloat;
  for(uint8_t i = 0; i < 4; i++) {
    PBytesToFloat.b[i] = data[i+2]; // 2, 3, 4, 5
    IBytesToFloat.b[i] = data[i+6]; // 6, 7, 8, 9
    DBytesToFloat.b[i] = data[i+10]; // 10, 11, 12, 13
  }
  if (data[1] == 0x00){ // position PID setting
    positionPID->P = PBytesToFloat.f;
    positionPID->I = IBytesToFloat.f;
    positionPID->D = DBytesToFloat.f;
  }
  else if (data[1] == 0x01) { // velocity PID setting
    velocityPID->P = PBytesToFloat.f;
    velocityPID->I = IBytesToFloat.f;
    velocityPID->D = DBytesToFloat.f;
  } else if (data[1] == 0x02) { // torque PID setting
    torquePID->P = PBytesToFloat.f;
    torquePID->I = IBytesToFloat.f;
    torquePID->D = DBytesToFloat.f;
  }
  RoboSnekController::updateFlash();
  irq_set_enabled(TIMER_IRQ_0, true);
  irq_set_enabled(TIMER_IRQ_1, true);
  irq_set_enabled(TIMER_IRQ_2, true);
  irq_set_enabled(TIMER_IRQ_3, true);
  return true;
}

bool RoboSnekController::calibrate(){
  irq_set_enabled(TIMER_IRQ_0, false);
  irq_set_enabled(TIMER_IRQ_1, false);
  irq_set_enabled(TIMER_IRQ_2, false);
  irq_set_enabled(TIMER_IRQ_3, false);
  calibrationRequest = false;
  if (feedback[0] != FEEDBACK_ERR_VAL && feedback[1] != FEEDBACK_ERR_VAL) {
    feedbackOffset[0] = rawAngle[0];
    feedbackOffset[1] = rawAngle[1];
  } else {
    irq_set_enabled(TIMER_IRQ_0, true);
    irq_set_enabled(TIMER_IRQ_1, true);
    irq_set_enabled(TIMER_IRQ_2, true);
    irq_set_enabled(TIMER_IRQ_3, true);
    return false;
  }
  RoboSnekController::updateFlash();
  irq_set_enabled(TIMER_IRQ_0, true);
  irq_set_enabled(TIMER_IRQ_1, true);
  irq_set_enabled(TIMER_IRQ_2, true);
  irq_set_enabled(TIMER_IRQ_3, true);
  return true;
}

bool RoboSnekController::controllerStep() {
  static float Sposition = 0;
  static float lastErrorPosition = 0;
  static float Svelocity = 0;
  static float lastErrorVelocity = 0;
  static float Storque = 0;
  static float lastErrorTorque = 0;

  if (controllerOnRequest && !overCurrent) {
    controllerOn = true;
  }
  controllerOnRequest = false;
  if(controllerOffRequest){
    controllerOn = false;
  }
  controllerOffRequest = false;
  if (!controllerOn) {
    RoboSnekController::updateFeedback( (uint)(100000/(((float) controlRate))) );
    controls[0] = 0;
    RoboSnekController::writeControls();
    Sposition = 0;
    lastErrorPosition = 0;
    Svelocity = 0;
    lastErrorVelocity = 0;
    Storque = 0;
    lastErrorTorque = 0;
    return true;
  }
  float lastFeedback[3] = {feedback[0],feedback[1],feedback[2]}; // last feedback
  if (!RoboSnekController::updateFeedback( (uint)(100000/(((float) controlRate)) ) )){
      if (overCurrent) {
        controllerOn = false;
        controls[0] = 0;
        RoboSnekController::writeControls();
        Sposition = 0;
        lastErrorPosition = 0;
        Svelocity = 0;
        lastErrorVelocity = 0;
        Storque = 0;
        lastErrorTorque = 0;
        return true;
      }


      if (controlledUnit == CONTROL_UNIT_POSITION) {
        Svelocity = 0;
        Storque = 0;
        lastErrorVelocity = 0;
        lastErrorTorque = 0;
        float error = setpoint[0] - feedback[0];
        if(error < -180.0) error += 360.0;
        if(error > 180.0) error -= 360.0;
        float deltaError = error - lastErrorPosition;
        lastErrorPosition = error;
        controls[0] = positionPID->P * error + positionPID->I*Sposition + positionPID->D*deltaError;

        if (controls[0] <= max_controls[1]){
          Sposition += error;
        }
        if (controls[0] >= max_controls[0]){
          Sposition += error;
        }
      } else if (controlledUnit == CONTROL_UNIT_VELOCITY) { //?? SANITY CHECK!
        Sposition = 0;
        Storque = 0;
        lastErrorPosition = 0;
        lastErrorTorque = 0;
        float error = setpoint[0] - (feedback[0] - lastFeedback[0])*controlRate; // estimate velocity: (x[k]-x[k-1])/T = (x[k]-x[k-1])*f
        float deltaError = error - lastErrorVelocity;
        lastErrorVelocity = error;
        controls[0] = velocityPID->P * error + velocityPID->I*Svelocity + velocityPID->D*deltaError;

        if (controls[0] <= max_controls[1]){
          Svelocity += error;
        }
        if (controls[0] >= max_controls[0]){
          Svelocity += error;
        }

      } else if (controlledUnit == CONTROL_UNIT_FORCE) {
        Sposition = 0;
        Svelocity = 0;
        lastErrorPosition = 0;
        lastErrorVelocity = 0;
        float error = setpoint[0] + (feedback[0] - feedback[1]); // estimate Torque
        if(error < -180.0) error += 360.0;
        if(error > 180.0) error -= 360.0;
        float deltaError = error - lastErrorTorque;
        lastErrorTorque = error;
        controls[0] = torquePID->P * error + torquePID->I*Storque + torquePID->D*deltaError;

        if (controls[0] <= max_controls[1]){
          Storque += error;
        }
        if (controls[0] >= max_controls[0]){
          Storque += error;
        }
      }
      // Safety stuff, It is neccessarry that controls > 0 DO INCREASE FEEDBACK and vice versa!
      /*if (feedback[0] >= max_states[1] && controls[0] > 0) {
        controls[0] = 0.0;
      }
      if (feedback[0] <= max_states[0] && controls[0] < 0) {
        controls[0] = 0.0;
      }*/
      RoboSnekController::writeControls();
      return true;
  }
  controls[0] = 0.0;
  RoboSnekController::writeControls();
  return true;
}
