//This file will contain controller implementation and robosnek object

#include "robosnek/controller.h"
#include "robosnek/robosnek.h"

#include "hardware/pwm.h"
#include "hardware/adc.h"
#include "hardware/uart.h"

#include <stdio.h>

#define PWM_CLK_FRAC 25,10 //AS IS! Format: denumerator,multiplyer
#define PWM_WRAP 20000

#define ADC_REF 3.3
#define SHUNT_RESISTANCE 0.12


#define CONTROLS_MAX 128.0 //I GUESS?
#define ABS_MAX_COTROLS 128.0

#define MOTOR_CURRENT_UPPER 0.500 // [A]
#define MOTOR_CURRENT_LOWER 0.400 // [A]

// RoboSnekController definition
RoboSnekController::RoboSnekController(uint8_t *outpins, uint8_t *inpins, uint64_t controlrate){
    // printf("Imma about to init wheelmodule\n");
    outPins = outpins;
    inPins = inpins;
    controlRate = controlrate;
    // init PWM
    gpio_set_function(outPins[0], GPIO_FUNC_PWM);
    gpio_set_function(outPins[1], GPIO_FUNC_PWM);
    uint slice_num = pwm_gpio_to_slice_num(outPins[0]);
    pwm_set_clkdiv_int_frac(slice_num, PWM_CLK_FRAC);
    pwm_set_wrap(slice_num, PWM_WRAP);
    slice_num = pwm_gpio_to_slice_num(outPins[1]);
    pwm_set_clkdiv_int_frac(slice_num, PWM_CLK_FRAC);
    pwm_set_wrap(slice_num, PWM_WRAP);


    //init ADC for current measurement
    #ifndef __ADC_DEFINED_ELSEWHERE__
    adc_init();
    #endif
    adc_gpio_init(inPins[0]);
    //ASSUMING NO CURRENT DURING CALIBRATION
    uint adc_reading = 0;
    adc_select_input(inPins[0] - 26);
    for (uint8_t i = 0; i<10;i++){
      adc_reading += adc_read();
      sleep_us(10);
    }
    adc_reading/=10;
    adcZeroOffset = adc_reading;

    // Init variables
    //feedback = new float[1](); // current
    //feedback[0] = 0;
    controls = new float[1]();
    //controls[0] = 0;
    setpoint = new float[1]();
    //setpoint[0] = 0;
    //states = new float[1](); // current
    //states[0] = 0;

    max_controls = new float[2];
    max_controls[0] = -CONTROLS_MAX;
    max_controls[1] = CONTROLS_MAX;
    //max_states = new float[2];
    //max_states[0] = -CURRENT_MAX;
    //max_states[1] =  CURRENT_MAX; // One day we will measure these
    //MotorsCurrent = new float[1];
    //MotorsCurrent[0] = 0;
    MotorsCurrent = new circularBuffer;
    MotorsCurrent->size = (int)(controlRate/2); // for 0.5 s of data
    MotorsCurrent->data = new float[MotorsCurrent->size]();
    MotorsCurrent->iterator = 0;
    MotorsCurrent->sum = 0;
    overCurrent = false;

    controllerOn = false;
    controllerOnRequest = false;
    controllerOffRequest = false;
    controlledUnit = CONTROL_UNIT_VELOCITY;

    RoboSnekController::enableOutput(true);
    RoboSnekController::writeControls();

}

void RoboSnekController::enableOutput(bool enable){
  uint slice_num = pwm_gpio_to_slice_num(outPins[0]);
  pwm_set_enabled(slice_num, enable);
  slice_num = pwm_gpio_to_slice_num(outPins[1]);
  pwm_set_enabled(slice_num, enable);
}

uint8_t RoboSnekController::updateFeedback(uint timeout_us){ //TODO MEASURE CURRENTS
  uint8_t ret_state = 0x00;
  // Sense current
  adc_select_input(inPins[0] - 26);
  uint adc_reading = 0;// - adcZeroOffset;
  for (uint8_t i = 0; i<100;i++){
    adc_reading += adc_read();
    sleep_us(1);
  }
  adc_reading/=100;
  if (adc_reading < adcZeroOffset) adc_reading = 0; //cap on 0 to prevent underflow
  else adc_reading-=adcZeroOffset;
  //printf("I read %i, %i\n",adc_reading, adcZeroOffset);
  //feedback[0] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;

  //MotorsCurrent[0] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;
  MotorsCurrent->sum -= MotorsCurrent->data[MotorsCurrent->iterator]; // substract previous data in buffer
  MotorsCurrent->data[MotorsCurrent->iterator] = (adc_reading/4095.0) * ADC_REF/SHUNT_RESISTANCE;
  MotorsCurrent->sum += MotorsCurrent->data[MotorsCurrent->iterator];
  MotorsCurrent->iterator = (((++MotorsCurrent->iterator) == MotorsCurrent->size)? 0 : MotorsCurrent->iterator);
  if (!overCurrent && MotorsCurrent->sum/MotorsCurrent->size > MOTOR_CURRENT_UPPER) {
    overCurrent = true;
  }
  else if(overCurrent && MotorsCurrent->sum/MotorsCurrent->size < MOTOR_CURRENT_LOWER){
    overCurrent = false;
  }
  //printf("about to control!\n");
  return ret_state;
}

void RoboSnekController::writeControls(){
  if (controls[0] < max_controls[0]){
    controls[0] = max_controls[0];
  }
  if (controls[0] > max_controls[1]){
    controls[0] = max_controls[1];
  }
  if (controls[0]>=0){
    uint16_t toSet = (uint16_t)(controls[0]/ABS_MAX_COTROLS*PWM_WRAP);
    //printf("about to write %i!\n", toSet);
    pwm_set_gpio_level(outPins[0], toSet);
    pwm_set_gpio_level(outPins[1], 0);
  } else {
    uint16_t toSet = (uint16_t)((-1*controls[0])/ABS_MAX_COTROLS*PWM_WRAP);
    //printf("about to write negative %i!\n", toSet);
    pwm_set_gpio_level(outPins[0], 0);
    pwm_set_gpio_level(outPins[1], toSet);
  }
}

uint8_t RoboSnekController::checkFeedback(){
  return 0x00;
}

float* RoboSnekController::getLastFeedback(){
    return feedback;
}
float* RoboSnekController::getStates(){
  return states;
}
float* RoboSnekController::getControls(){
  return controls;
}

bool RoboSnekController::controllerStep() {
  //printf("Imma control\n");
  if (controllerOnRequest && !overCurrent) {
    controllerOn = true;
    controllerOnRequest = false;
  }
  if(controllerOffRequest){
    controllerOn = false;
    controllerOffRequest = false;
  }
  if (!controllerOn) {
    RoboSnekController::updateFeedback( (uint)(100000/(((float) controlRate)) ) );
    controls[0] = 0;
    RoboSnekController::writeControls();
    return true;
  }

  if (!RoboSnekController::updateFeedback( (uint)(100000/(((float) controlRate)) ) )){
      //printf("controlling\n");
      if (overCurrent) {
        controllerOn = false;
        controls[0] = 128;
        RoboSnekController::writeControls();
        return true;
      }
      controls[0] = setpoint[0]-128;
      RoboSnekController::writeControls();
      return true;
  }
  //printf("Update feedback error\n");
  controls[0] = 0;
  RoboSnekController::writeControls();
  return true;
}
