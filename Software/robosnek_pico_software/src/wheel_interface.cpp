/*
  This file contains specific part of interface protocol for wheel module.
*/
#include <cstdlib>
#include "robosnek/interface.h"

void RoboSnekInterface::react(){
  switch (dataForMe[0]) {
    case MSG_COMMAND_SET_ADDRESS:
      myAddress = dataForMe[1];
      myAddress++;
      dataForMe[1]++;
      break;
    case MSG_COMMAND_SET_POSITION:
      // do nothing
      uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
      uart_putc_raw(uartToMaster,3);
      uart_putc_raw(uartToMaster,myAddress);
      uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
      uart_putc_raw(uartToMaster,MSG_ERROR_UNSUPORTED_COMMAND);
      break;
    case MSG_COMMAND_SET_VELOCITY:
      Controller->setpoint[0] = (float)dataForMe[1]; // TODO Some max velocity? How to include?
      Controller->controlledUnit = CONTROL_UNIT_VELOCITY;
      Controller->controllerOnRequest = true;
      break;
    case MSG_COMMAND_SET_FORCE:
      // Do nothing
      uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
      uart_putc_raw(uartToMaster,3);
      uart_putc_raw(uartToMaster,myAddress);
      uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
      uart_putc_raw(uartToMaster,MSG_ERROR_UNSUPORTED_COMMAND);
      break;
    case MSG_COMMAND_KILL:
      Controller->controllerOffRequest = true;
      break;
    case MSG_COMMAND_GET_CURRENT:
      //uint8_t current = (uint8_t)(2*Controller->MotorsCurrent[0]/1); // Feedback/0.5
      uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
      uart_putc_raw(uartToMaster,3);
      uart_putc_raw(uartToMaster,myAddress);
      uart_putc_raw(uartToMaster,MSG_COMMAND_GET_CURRENT);
      uart_putc_raw(uartToMaster,(uint8_t)(Controller->MotorsCurrent->data[Controller->MotorsCurrent->iterator]*255));
      break;
    case MSG_COMMAND_GET_ANGLES:
      if (uart_is_writable(uartToMaster)){
            //send Error
            uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
            uart_putc_raw(uartToMaster,3);
            uart_putc_raw(uartToMaster,myAddress);
            uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
            uart_putc_raw(uartToMaster,MSG_ERROR_UNSUPORTED_COMMAND);
        }
      break;
    case MSG_COMMAND_SET_PID:
      if (uart_is_writable(uartToMaster)){
            //send Error
            uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
            uart_putc_raw(uartToMaster,3);
            uart_putc_raw(uartToMaster,myAddress);
            uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
            uart_putc_raw(uartToMaster,MSG_ERROR_UNSUPORTED_COMMAND);
        }
      break;
    case MSG_COMMAND_SET_ANGLE_ZERO_POINT:
      if (uart_is_writable(uartToMaster)){
            //send Error
            uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
            uart_putc_raw(uartToMaster,3);
            uart_putc_raw(uartToMaster,myAddress);
            uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
            uart_putc_raw(uartToMaster,MSG_ERROR_UNSUPORTED_COMMAND);
        }
      break;
  }
}
