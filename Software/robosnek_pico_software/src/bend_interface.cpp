/*
  This file contains specific part of interface protocol for bend module.
*/
#include <cstdlib>
#include <cstdio>
#include "robosnek/interface.h"

void RoboSnekInterface::react(){
  union {
    float f;
    uint8_t b[4];
  } someBytesToFloat;
  switch (dataForMe[0]) {
    case MSG_COMMAND_SET_ADDRESS:
      myAddress = dataForMe[1];
      myAddress++;
      dataForMe[1]++;
      break;
    case MSG_COMMAND_SET_POSITION:
      // TODO: Change da shit to -180 to 180 version where 128 = 0deg
      Controller->setpoint[0] = (dataForMe[1])* 360.0/255 ;
      Controller->controlledUnit = CONTROL_UNIT_POSITION;
      Controller->controllerOnRequest = true;
      break;
    case MSG_COMMAND_SET_VELOCITY: // This functionality is not yet available
      break;
      Controller->setpoint[0] = 0; //(float)dataForMe[1]; // TODO Some max velocity? How to include?
      Controller->controlledUnit = CONTROL_UNIT_VELOCITY;
      Controller->controllerOnRequest = true;
      break;
    case MSG_COMMAND_SET_FORCE:
      Controller->setpoint[0] = (float)(128-dataForMe[1]); // TODO Some max force? How to include?
      Controller->controlledUnit = CONTROL_UNIT_FORCE;
      Controller->controllerOnRequest = true;
      break;
    case MSG_COMMAND_KILL:
      Controller->controllerOffRequest = true;
      break;
    case MSG_COMMAND_GET_CURRENT:
      //uint8_t current = (uint8_t)(2*Controller->MotorsCurrent[0]/1); // Feedback/0.5
      uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
      uart_putc_raw(uartToMaster,3);
      uart_putc_raw(uartToMaster,myAddress);
      uart_putc_raw(uartToMaster,MSG_COMMAND_GET_CURRENT);
      uart_putc_raw(uartToMaster,(uint8_t)(Controller->MotorsCurrent->data[Controller->MotorsCurrent->iterator]*255));
      break;
    case MSG_COMMAND_GET_ANGLES:
      if (uart_is_writable(uartToMaster)){
          // DONE edit Controller feedback from floats to uint8_t and send
          float *feedback = Controller->getLastFeedback();
          if (feedback[0] == -1 || feedback[1] == -1){
            //send Error
            uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
            uart_putc_raw(uartToMaster,3);
            uart_putc_raw(uartToMaster,myAddress);
            uart_putc_raw(uartToMaster,MSG_COMMAND_ERROR_OCCURED);
            uart_putc_raw(uartToMaster,MSG_ERROR_FEEDBACK_ERROR);
          }else{
            //TODO: Shit can be negative!
            uint8_t position1 = (uint8_t)(feedback[0]/360*255);
            uint8_t position2 = (uint8_t)(feedback[1]/360*255);
            uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
            uart_putc_raw(uartToMaster,4);
            uart_putc_raw(uartToMaster,myAddress);
            uart_putc_raw(uartToMaster,MSG_COMMAND_GET_ANGLES);
            uart_putc_raw(uartToMaster,position1);
            uart_putc_raw(uartToMaster,position2);
        }
      }
      break;
    case MSG_COMMAND_SET_PID:
      Controller->setPID(dataForMe);
      if (uart_is_writable(uartToMaster)){
        uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
        uart_putc_raw(uartToMaster,2);
        uart_putc_raw(uartToMaster,myAddress);
        uart_putc_raw(uartToMaster,MSG_COMMAND_SET_PID);
      }
      break;
  case MSG_COMMAND_SET_ANGLE_ZERO_POINT:
    Controller->calibrationRequest = true;
    if (Controller->calibrate()) {
      if (uart_is_writable(uartToMaster)){
        uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
        uart_putc_raw(uartToMaster,2);
        uart_putc_raw(uartToMaster,myAddress);
        uart_putc_raw(uartToMaster,MSG_COMMAND_SET_ANGLE_ZERO_POINT);
      }
    } else {
      if (uart_is_writable(uartToMaster)){
        uart_putc_raw(uartToMaster,MSG_MASTER_ADDRESS);
        uart_putc_raw(uartToMaster,2);
        uart_putc_raw(uartToMaster,myAddress);
        uart_putc_raw(uartToMaster,MSG_ERROR_FEEDBACK_ERROR);
      }
    }
    break;
  }
}
