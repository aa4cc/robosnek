/*
* This project was founded as a
* team project at CTU in Prag
* by team RoboSnek.
*/

//This file will contain running app for bending module of RoboSnek

#include <stdio.h>
#include "pico/stdlib.h"

#include "hardware/adc.h"
#include "hardware/dma.h"
#include "hardware/pwm.h"
#include "hardware/irq.h"

#include "robosnek/controller.h"
#include "robosnek/interface.h"

#define UART_BAUD_RATE 115200
#define UART_DATA_BITS 8
#define UART_STOP_BITS 1
#define UART_PARITY UART_PARITY_NONE

//#define UART0_TX 17
//#define UART0_RX 16
#define UART0_TX 12
#define UART0_RX 13
#define UART1_TX 8
#define UART1_RX 9

#define PWM_POSITIVE_GPIO 0
#define PWM_NEGATIVE_GPIO 1
#define I2C1_SCL_PIN 3
#define I2C1_SDA_PIN 2
#define I2C0_SCL_PIN 5
#define I2C0_SDA_PIN 4
#define ADC_CURRENT_SENSE 26 //ADC0 pin 31

bool controllerStep(repeating_timer_t *rt) {
  return ((RoboSnekController *)(rt->user_data))->controllerStep();
}

void init_uart();

int main(){
  stdio_init_all();
  uint LED_PIN = PICO_DEFAULT_LED_PIN;
  gpio_init(LED_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);
  gpio_put(LED_PIN, 0);

  uint8_t *out_pins = new uint8_t[2];
  out_pins[0] = PWM_POSITIVE_GPIO;
  out_pins[1] = PWM_NEGATIVE_GPIO;
  uint8_t *in_pins = new uint8_t[5];
  in_pins[0] = I2C0_SCL_PIN;
  in_pins[1] = I2C0_SDA_PIN;
  in_pins[2] = I2C1_SCL_PIN;
  in_pins[3] = I2C1_SDA_PIN;
  in_pins[4] = ADC_CURRENT_SENSE;
  int hz = 1000;
  sleep_ms(5000);
  RoboSnekController controller(out_pins, in_pins, hz); // ControlRate = 100 Hz
  repeating_timer_t timer;
  timer.user_data = (void *) &controller;
  controller.setpoint[0] = 180.0;
  add_repeating_timer_us(-1000000 / hz, controllerStep, timer.user_data, &timer); // a bit high level, might cause issues

  uint8_t *uart0Pins = new uint8_t[2];
  uart0Pins[0] = UART0_RX;
  uart0Pins[1] = UART0_TX;
  uint8_t *uart1Pins = new uint8_t[2];
  uart1Pins[0] = UART1_RX;
  uart1Pins[1] = UART1_TX;
  RoboSnekInterface interface(uart0Pins, uart1Pins, &controller, UART_BAUD_RATE, UART_DATA_BITS, UART_PARITY, UART_STOP_BITS);

  float *feedback;
  float *controls;


  gpio_put(LED_PIN, 1);
  while(1){
    interface.checkState();
    //TODO change setpoint signal to move the servo and send statistics over usb?
    feedback = controller.getLastFeedback();
    //controls = controller.getControls();
    //printf("Current Angle = %0.3f, Current second Angle = %0.3f, Current Setpoint = %0.3f and Controlls = %0.3f\r", feedback[0],
  //                                                                                                                  feedback[1],
    //                                                                                                                controller.setpoint[0],
    //                                                                                                                controls[0]);
    //printf("Current Angle = %0.3f, Current second Angle = %0.3f, Torque= %0.3f\r", feedback[0],  feedback[1], feedback[0]+feedback[1]-360.0);
    //printf("Current Setpoint %.3f\r", controller.setpoint[0]);
    //printf("Current current %.3f\r", feedback[2]);
    //printf("Position: %.3f, %.3f, %.3f\n", controller.positionPID->P,controller.positionPID->I,controller.positionPID->D);
    //printf("Velocity: %.3f, %.3f, %.3f\n", controller.velocityPID->P,controller.velocityPID->I,controller.velocityPID->D);
    //printf("Torque: %.3f, %.3f, %.3f\n", controller.torquePID->P,controller.torquePID->I,controller.torquePID->D);
    //sleep_ms(10);

    /*gpio_put(LED_PIN, 1);
    sleep_ms(100);
    gpio_put(LED_PIN, 0);
    sleep_ms(100);*/

  }
  return 0;
}
