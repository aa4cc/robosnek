/*
  This file contains common part of interface protocol.
*/
#include <cstdlib>
#include <stdio.h>
#include "robosnek/interface.h"
#include "pico/stdlib.h"




RoboSnekInterface::RoboSnekInterface(uint8_t* pinsUart0, uint8_t* pinsUart1, RoboSnekController* controller, uint baudRate, uint databits, uart_parity_t parity, uint stopBits){
  // TODO: set up both usarts
  // printf("About to init\n");
  uart_init(uart0, baudRate);
  gpio_set_function(pinsUart0[0], GPIO_FUNC_UART);
  gpio_set_function(pinsUart0[1], GPIO_FUNC_UART);
  uart_set_hw_flow(uart0, false, false);
  uart_set_format(uart0, databits, stopBits, parity);
  uart_set_fifo_enabled(uart0, true);
  // printf("first initialized\n");
  uart_init(uart1, baudRate);
  gpio_set_function(pinsUart1[0], GPIO_FUNC_UART);
  gpio_set_function(pinsUart1[1], GPIO_FUNC_UART);
  uart_set_hw_flow(uart1, false, false);
  // printf("About to init\n");
  uart_set_format(uart1, databits, stopBits, parity);
  uart_set_fifo_enabled(uart1, true);
  // printf("Second initialized\n");

  // Init variables.
  header[0] = 123;
  header[1] = 124;
  dataForMe = NULL;

  Controller = controller;
  uartToMaster = NULL;
  uartFromMaster = NULL;
  comState = WAIT_FOR_START;
  myAddress = 0xFF;
  sleep_ms(1);
  // flush the FIFO
  // printf("About to flush Fifos\n");
  while(uart_is_readable(uart0)) {
    // printf("clearing0\n");
    uart_getc(uart0);
  }
  while(uart_is_readable(uart1)){
    // printf("clearing1\n");
    uart_getc(uart1);
  }
  // printf("Pls start sending Lucy\n");
}

uint8_t RoboSnekInterface::checkState(){
  // check usart flags and react, retrun current state of comunication as state machine

  // TODO chceck state of incomming messages
  //printf("My Address is %i\r", myAddress);
  static uint32_t prevTimeStamp = 0;
  bool readable = false;
  char c;
  switch (comState) {

    // Run Stage
    case WAIT_FOR_START: // It is expected that first message comes from master!
      if (uartToMaster == NULL && uart_is_readable(uart0)){
        uartToMaster = uart0;
        uartFromMaster = uart1;
      } else if (uartToMaster == NULL && uart_is_readable(uart1)){
        uartToMaster = uart1;
        uartFromMaster = uart0;
      }

      if (uartToMaster != NULL && uart_is_readable(uartToMaster)){
        header[0] = uart_getc(uartToMaster);
        prevTimeStamp = time_us_32();
        if (header[0] == myAddress || header[0] == MSG_ADDR_BROADCAST) {
          // printf("This message is for me %i, I am %i\n", header[0], myAddress);
          comState = WAIT_FOR_LENGTH;
        } else {
          comState = WAIT_FOR_LENGTH;
          // printf("This message aint for me %i, I am %i\n", header[0], myAddress);
          if (uart_is_writable(uartFromMaster)){
            uart_putc_raw(uartFromMaster, header[0]);
          }
        }
      }
      break;
    case WAIT_FOR_LENGTH:
      if (uart_is_readable(uartToMaster)){
        header[1] = uart_getc(uartToMaster);
        prevTimeStamp = time_us_32();
        // printf("Message has %i bytes\n", header[1]);
        if (header[0] == myAddress || header[0] == MSG_ADDR_BROADCAST) {
          comState = WAIT_FOR_DATA;
        } else {
          comState = WAIT_FOR_DATA;
          if (uart_is_writable(uartFromMaster)){
            uart_putc_raw(uartFromMaster, header[1]);
          }
        }
        bytesSent = 0;
      } else {
        uint32_t timeStamp = time_us_32();
        if (timeStamp-prevTimeStamp > 1000000) {
          comState = WAIT_FOR_START;
        }
      }
      break;

     case WAIT_FOR_DATA:
      if (uart_is_readable(uartToMaster)){
        prevTimeStamp = time_us_32();
        // printf("Shitstorm incomming\n");
        if (header[0] == myAddress || header[0] == MSG_ADDR_BROADCAST){
          if (bytesSent == 0){
            if(dataForMe != NULL) free(dataForMe);
            dataForMe = (uint8_t*) malloc(header[1]*sizeof(uint8_t));
          }
          dataForMe[bytesSent] = uart_getc(uartToMaster);
        } else {
          if (uart_is_writable(uartFromMaster)){
            uart_putc_raw(uartFromMaster, uart_getc(uartToMaster));
          }
        }
        bytesSent++;
        if ((header[0] == myAddress || header[0] == MSG_ADDR_BROADCAST) && bytesSent == header[1]){
          react();
          // printf("I reacted\n");
          if (header[0] == MSG_ADDR_BROADCAST){ // resends after reaction if broadcast
            // resend header and msg
            if (uart_is_writable(uartFromMaster)){
              // printf("I resend broadcast\n");
              uart_putc_raw(uartFromMaster,header[0]);
              uart_putc_raw(uartFromMaster,header[1]);
              for (uint8_t i = 0; i < header[1]; i++){
                uart_putc_raw(uartFromMaster,dataForMe[i]);
              }
            }
          }
          comState = WAIT_FOR_START;
        } else if (bytesSent == header[1]) {
          comState = WAIT_FOR_START;
        }
      } else {
        uint32_t timeStamp = time_us_32();
        if (timeStamp-prevTimeStamp > 1000000) {
          comState = WAIT_FOR_START;
        }
      }
      break;

    }
    // And always repeat data for master
    //printf("comState is %i\n", comState);
    //if (comState >= WAIT_FOR_START ){
    if (uartFromMaster != NULL && uart_is_readable(uartFromMaster)){
      //printf("Is there something on the onther side?\n");
      if (uart_is_writable(uartToMaster)) {
        //printf("there is\n");
        uart_putc_raw(uartToMaster, uart_getc(uartFromMaster));
      }
    //}
    }

    return comState;
  }
