#include "pico/stdlib.h"
#include "hardware/pwm.h"
#include "hardware/irq.h"

void on_pwm_wrap() {
    static int fade = 0;
    static bool going_up = true;
    // Clear the interrupt flag that brought us here
    pwm_clear_irq(pwm_gpio_to_slice_num(0));

    if (going_up) {
        ++fade;
        if (fade > 500) {
            fade = 500;
            going_up = false;
        }
    } else {
        --fade;
        if (fade < 70) {
            fade = 70;
            going_up = true;
        }
    }
    // Square the fade value to make the LED's brightness appear more linear
    // Note this range matches with the wrap value
    pwm_set_gpio_level(0, fade);
}


int main() {
  // Tell GPIO 0 and 1 they are allocated to the PWM
  gpio_set_function(0, GPIO_FUNC_PWM);
  gpio_set_function(1, GPIO_FUNC_PWM);

  // Find out which PWM slice is connected to GPIO 0 (it's slice 0)
  uint slice_num = pwm_gpio_to_slice_num(0);

  // Interrupts
  pwm_clear_irq(slice_num);
  pwm_set_irq_enabled(slice_num, true);
  irq_set_exclusive_handler(PWM_IRQ_WRAP, on_pwm_wrap);
  irq_set_enabled(PWM_IRQ_WRAP, true);

  // set divider to 1:256
  pwm_set_clkdiv_int_frac(slice_num, 255,1);
  // Set period of 4 cycles (0 to 1000 inclusive)
  pwm_set_wrap(slice_num, 1000);

  //This part is for PWM without interrupts
  // Set channel A output high for one cycle before dropping
  //pwm_set_chan_level(slice_num, PWM_CHAN_A, 500);
  // Set initial B output high for three cycles before dropping
  //pwm_set_chan_level(slice_num, PWM_CHAN_B, 0);
  // Set the PWM running
  //pwm_init(slice_num, &config, true);
  pwm_set_enabled(slice_num, true);
  while (1); //endless loop
}
