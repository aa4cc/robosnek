/**
 * Copyright (c) 2021 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"

#include "hardware/adc.h"
#include "hardware/dma.h"
#include "hardware/pwm.h"
#include "hardware/irq.h"

#define POSITION_CHANNEL 0
#define CAPTURE_BUFFER_SIZE 10
#define CLK_DIV 24
#define FIRST_TO_READ 0
#define ROBIN_MSK 0b00001

#define PWM_PINP 0
#define PWM_PINN 1
#define PWM_DIV 255
#define PWM_PERIOD 1000

uint16_t capture_buf[CAPTURE_BUFFER_SIZE];
int dma_chan;
uint slice_num;
const uint LED_PIN = PICO_DEFAULT_LED_PIN;
float setpoint = 4095/2;

void controller_step(){
  pwm_clear_irq(slice_num);
  adc_fifo_drain();
  dma_channel_set_trans_count(dma_chan, CAPTURE_BUFFER_SIZE,false);
  dma_channel_set_read_addr(dma_chan, &adc_hw->fifo, false);
  dma_channel_set_write_addr(dma_chan, capture_buf, false);
  dma_channel_start(dma_chan);
  adc_run(true);
  dma_channel_wait_for_finish_blocking(dma_chan);
  adc_run(false);

  float mean_pos=0;
  for (int i = 0; i < CAPTURE_BUFFER_SIZE; i++) {
      mean_pos += capture_buf[i];
  }
  mean_pos /= CAPTURE_BUFFER_SIZE;

  // control
  float error = setpoint - mean_pos;
  float out = 0.2*error;
  printf("error %0.3f\n",error);
  printf("out %0.3f\n",out);
  if (out >= 0){
    if (out > PWM_PERIOD){
      out = PWM_PERIOD;
    }
    uint16_t set = out;
    pwm_set_enabled(slice_num, false);
    pwm_set_gpio_level(PWM_PINP, set);
    pwm_set_gpio_level(PWM_PINN, 0);
    pwm_set_enabled(slice_num, true);
  } else {
    if (out < -PWM_PERIOD){
      out = -PWM_PERIOD;
    }
    uint16_t set = (-out);
    pwm_set_enabled(slice_num, false);
    pwm_set_gpio_level(PWM_PINP, 0);
    pwm_set_gpio_level(PWM_PINN, set);
    pwm_set_enabled(slice_num, true);

  }


}

void init_adc_dma(){
  // Init GPIO for analogue use: hi-Z, no pulls, disable digital input buffer.
  adc_init();
  adc_gpio_init(26 + POSITION_CHANNEL);
  adc_set_temp_sensor_enabled(false);
  //adc_select_input(0);
  adc_set_round_robin(ROBIN_MSK);
  adc_fifo_setup(
      true,    // Write each completed conversion to the sample FIFO
      true,    // Enable DMA data request (DREQ)
      1,       // DREQ (and IRQ) asserted when at least 1 sample present
      false,   // We won't see the ERR bit because of 8 bit reads; disable.
      false     // Shift each sample to 8 bits when pushing to FIFO
  );

  adc_set_clkdiv(CLK_DIV);

  dma_chan = dma_claim_unused_channel(true);
  dma_channel_config cfg = dma_channel_get_default_config(dma_chan);
  // Set up the DMA to start transferring data as soon as it appears in FIFO
  // Reading from constant address, writing to incrementing byte addresses
  channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
  channel_config_set_read_increment(&cfg, false);
  channel_config_set_write_increment(&cfg, true);

  // Pace transfers based on availability of ADC samples
  channel_config_set_dreq(&cfg, DREQ_ADC);
    dma_channel_configure(dma_chan, &cfg,
      capture_buf,    // dst
      &adc_hw->fifo,  // src
      CAPTURE_BUFFER_SIZE,  // transfer count
      false            // start immediately
  );
}

void init_pwm_irq(){
  // Find out which PWM slice is connected to PWM_PIN
  gpio_set_function(PWM_PINP, GPIO_FUNC_PWM);
  gpio_set_function(PWM_PINN, GPIO_FUNC_PWM);
  slice_num = pwm_gpio_to_slice_num(PWM_PINP);

  // Interrupts
  pwm_clear_irq(slice_num);
  pwm_set_irq_enabled(slice_num, true);
  irq_set_exclusive_handler(PWM_IRQ_WRAP, controller_step);
  irq_set_enabled(PWM_IRQ_WRAP, true);

  // set divider
  pwm_set_clkdiv_int_frac(slice_num, PWM_DIV,1);
  pwm_set_wrap(slice_num, PWM_PERIOD);


  pwm_set_enabled(slice_num, true);

  // Full stop
  pwm_set_gpio_level(PWM_PINP, 0);
  pwm_set_gpio_level(PWM_PINN, 0);


}



int main() {
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, 0);
    stdio_init_all();
    init_adc_dma();
    init_pwm_irq();
    //controller_step();
    while(1){

  }
}
