#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"

#define I2C_SCL_PIN 5
#define I2C_SDA_PIN 4

#define ENC_ADDR 0b0110110
#define ENC_ADDR_READ 0b01101101
#define ENC_ADDR_WRITE 0b01101100
#define ENC_MASK_MD 0b00100000
#define ENC_MASK_ML 0b00010000
#define ENC_MASK_MH 0b00001000
const uint8_t ENC_ANGLE_REG = 0x0E;
const uint8_t ENC_STATUS_REG = 0x0B;

int main() {
    stdio_init_all();
    i2c_init(i2c0, 48000);
    gpio_set_function(I2C_SDA_PIN, GPIO_FUNC_I2C);
    gpio_set_function(I2C_SCL_PIN, GPIO_FUNC_I2C);
    sleep_ms(10000);
    printf("I2C should be inited \n");
    uint8_t data[2];
    uint8_t status;
    uint16_t angle;
    while(1){
      //i2c_write_blocking(i2c0, ENC_ADDR, &ENC_ANGLE_REG, 1, true);
      //i2c_read_blocking(i2c0, ENC_ADDR, data, 2, false);
      //angle = (data[0]<<8)|data[1];
      //printf("I just read angle of %i\n",angle);
      i2c_write_blocking(i2c0, ENC_ADDR, &ENC_STATUS_REG, 1, true);
      i2c_read_blocking(i2c0, ENC_ADDR, &status, 1, false);
      if(status&ENC_MASK_MD){
        printf("Magnet detected\n");
      } else {
        printf("Magnet WAS NOT DETECTED\n");
      }
      if(status&ENC_MASK_MH){
        printf("Magnet TOO HIGH\n");
      } else {
        printf("Magnet not too high\n");
      }
      if(status&ENC_MASK_ML){
        printf("Magnet TOO LOW\n");
      } else {
        printf("Magnet not too low\n");
      }

      sleep_ms(100);
  }
  return 0;
}
