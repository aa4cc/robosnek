/**
 * Copyright (c) 2021 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"
// For ADC input:
#include "hardware/adc.h"
#include "hardware/dma.h"
#include "hardware/irq.h"
// Channel 0 is GPIO26
#define CAPTURE_CHANNEL 0
#define CAPTURE_BUFFER_SIZE 2000
#define CAPTURE_DEPTH 1000
#define CLK_DIV 24
#define FIRST_TO_READ 0
#define ROBIN_MSK 0b10001

uint16_t capture_buf[CAPTURE_BUFFER_SIZE];
int dma_chan;


void dma_handler(){
  adc_run(false);
  adc_fifo_drain();

  // get mean
  float mean_pos=0;
  float mean_temp=0;
  for (int i = 0; i < CAPTURE_BUFFER_SIZE; i = i+2) {
      mean_pos += capture_buf[i];
      mean_temp += capture_buf[i+1];
  }
  mean_temp /= CAPTURE_DEPTH;
  mean_pos /= CAPTURE_DEPTH;

  printf("%.2f  %.2f \r", mean_pos, mean_temp);

  dma_hw->ints0 = 1u << dma_chan; // turn off the irq flag
  dma_channel_set_read_addr(dma_chan, &adc_hw->fifo, false);
  dma_channel_set_write_addr(dma_chan, capture_buf, false);
  dma_channel_start(dma_chan);
  adc_fifo_drain();
  adc_select_input(FIRST_TO_READ);
  adc_run(true);
  return;
}


void init_adc_dma_irq(){
  // Init GPIO for analogue use: hi-Z, no pulls, disable digital input buffer.
  adc_init();
  adc_gpio_init(26 + CAPTURE_CHANNEL);
  adc_set_temp_sensor_enabled(true);
  //adc_select_input(0);
  adc_set_round_robin(ROBIN_MSK);
  adc_fifo_setup(
      true,    // Write each completed conversion to the sample FIFO
      true,    // Enable DMA data request (DREQ)
      1,       // DREQ (and IRQ) asserted when at least 1 sample present
      false,   // We won't see the ERR bit because of 8 bit reads; disable.
      false     // Shift each sample to 8 bits when pushing to FIFO
  );
  // Divisor of 0 -> full speed. Free-running capture with the divider is
  // equivalent to pressing the ADC_CS_START_ONCE button once per `div + 1`
  // cycles (div not necessarily an integer). Each conversion takes 96
  // cycles, so in general you want a divider of 0 (hold down the button
  // continuously) or > 95 (take samples less frequently than 96 cycle
  // intervals). This is all timed by the 48 MHz ADC clock.
  adc_set_clkdiv(CLK_DIV);

  dma_chan = dma_claim_unused_channel(true);
  dma_channel_config cfg = dma_channel_get_default_config(dma_chan);
  // Set up the DMA to start transferring data as soon as it appears in FIFO
  // Reading from constant address, writing to incrementing byte addresses
  channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
  channel_config_set_read_increment(&cfg, false);
  channel_config_set_write_increment(&cfg, true);

  // Pace transfers based on availability of ADC samples
  channel_config_set_dreq(&cfg, DREQ_ADC);
    dma_channel_configure(dma_chan, &cfg,
      capture_buf,    // dst
      &adc_hw->fifo,  // src
      CAPTURE_BUFFER_SIZE,  // transfer count
      false            // start immediately
  );
  // Tell the DMA to raise IRQ line 0 when the channel finishes a block
  dma_channel_set_irq0_enabled(dma_chan, true);
  // Configure the processor to run dma_handler() when DMA IRQ 0 is asserted
  irq_set_exclusive_handler(DMA_IRQ_0, dma_handler);
  irq_set_enabled(DMA_IRQ_0, true);
  dma_hw->ints0 = 1u << dma_chan;
}


int main() {
    stdio_init_all();
    init_adc_dma_irq();
    dma_handler();
    while(1) {
  }
}
