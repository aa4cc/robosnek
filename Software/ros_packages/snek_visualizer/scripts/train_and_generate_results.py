import gym
# from agent import TRPOAgent
import snek_driving
from stable_baselines3.common.evaluation import evaluate_policy

import time
import timeit

from stable_baselines3 import PPO
from sb3_contrib import RecurrentPPO
from stable_baselines3 import A2C
from stable_baselines3 import DDPG
from stable_baselines3 import SAC

from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.vec_env import DummyVecEnv, VecNormalize

import os

import numpy as np
import matplotlib.pyplot as plt

# from stable_baselines3.ddpg.policies import LnMlpPolicy
# from stable_baselines3.common.noise import AdaptiveParamNoiseSpec
from stable_baselines3.common import results_plotter
# from stable_baselines3.bench import Monitor
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.callbacks import BaseCallback


class SaveOnBestTrainingRewardCallback(BaseCallback):
    """
    Callback for saving a model (the check is done every ``check_freq`` steps)
    based on the training reward (in practice, we recommend using ``EvalCallback``).

    :param check_freq: (int)
    :param log_dir: (str) Path to the folder where the model will be saved.
      It must contains the file created by the ``Monitor`` wrapper.
    :param verbose: (int)
    """
    def __init__(self, check_freq: int, log_dir: str, verbose=1, save_freq = 5):
        super(SaveOnBestTrainingRewardCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        # self.save_path = os.path.join(log_dir, 'best_model')
        self.save_path = log_dir + "latest_model"
        self.best_mean_reward = -np.inf
        self.n_episodes_done = 0
        self.save_freq = save_freq

    def _init_callback(self) -> None:
        # Create folder if needed
        if self.save_path is not None:
            os.makedirs(self.save_path, exist_ok=True)

    def _on_step(self) -> bool:
        if self.n_calls % self.check_freq == 0:
            self.n_episodes_done += 1
            print("EPISODE: ",self.n_episodes_done)

            # Retrieve training reward
            x, y = ts2xy(load_results(self.log_dir), 'timesteps')
            if len(x) > 0:
                # Mean training reward over the last 5 episodes
                mean_reward = np.mean(y[-5:])

                last_reward = y[-1]
                if self.verbose > 0:
                    print("Num timesteps: {}".format(self.num_timesteps))
                print("Best mean reward: {:.2f} - Last mean reward per episode: {:.2f}, last total reward: {:.2f}".format(self.best_mean_reward, mean_reward, last_reward))

                # New best model, you could save the agent here
                # if mean_reward > self.best_mean_reward:
                if self.n_episodes_done % self.save_freq == 0:
                    self.best_mean_reward = mean_reward
                    # Example for saving best model
                    if self.verbose > 0:
                        current_save_path = self.save_path + "_ep_" + str(self.n_episodes_done)
                        print("Saving latest model to {}".format(current_save_path))
                        self.model.save(current_save_path)

        return True


if __name__ == '__main__':

    #CHANGE THE TRAINING PARAMETERS HERE
    max_sim_steps_per_episode = 1000
    num_episodes = 10

    #TERRAIN SELECTION
    terrain = "stairs" 
    # terrain = "bumps" 

    #ROBOT CONFIG SELECTION (1st one is for stairs, 2nd one for bumps, but you can play around with it)
    bc = [[1,0,0], [0,0,0], [0,0,0], [1,0,0], [0,1,0], [0,0,0], [1,1,0]] 
    # bc = [[1,0,0], [0,1,0], [0,0,0], [1,1,0]] 

    # Create models dir
    log_dir = "models/"
    os.makedirs(log_dir, exist_ok=True)

    # Create and wrap the environment
    env = gym.make('SnekDriving-v0', body_config=bc, gui = True, max_num_simulation_steps = max_sim_steps_per_episode, terrain_type = terrain )
    env = Monitor(env, log_dir)

    model = PPO("MlpPolicy", env, verbose=0)
    # model = A2C("MlpPolicy", env, verbose=0)
    # model = DDPG(LnMlpPolicy, env, param_noise=param_noise, verbose=0)

    # Create the callback: check every 1000 steps
    callback = SaveOnBestTrainingRewardCallback(check_freq=max_sim_steps_per_episode, log_dir=log_dir)
    # Train the agent
    time_steps = num_episodes * max_sim_steps_per_episode
    model.learn(total_timesteps=int(time_steps), callback=callback)

    # results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "DDPG LunarLander")
    results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "Bumpy terrain learning")
    plt.show()

    print("------------LEARNING DONE")
