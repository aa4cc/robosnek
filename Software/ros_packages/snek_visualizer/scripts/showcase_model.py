import gym
import torch
import sys
# from agent import TRPOAgent
import pybullet_envs
import snek_driving
from stable_baselines3.common.evaluation import evaluate_policy

import time
import timeit

from stable_baselines3 import PPO
from stable_baselines3 import A2C
from stable_baselines3 import DDPG
from stable_baselines3 import SAC

from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.vec_env import DummyVecEnv, VecNormalize

import os

import numpy as np
import matplotlib.pyplot as plt

# from stable_baselines3.ddpg.policies import LnMlpPolicy
# from stable_baselines3.common.noise import AdaptiveParamNoiseSpec
from stable_baselines3.common import results_plotter
# from stable_baselines3.bench import Monitor
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.callbacks import BaseCallback


if __name__ == '__main__':

    #CHANGE THE TRAINING PARAMETERS HERE
    max_sim_steps_per_episode = 1000
    num_episodes = 10

    #TERRAIN SELECTION
    terrain = "stairs" 
    # terrain = "bumps" 

    #ROBOT CONFIG SELECTION (1st one is for stairs, 2nd one for bumps, but you can play around with it)
    bc = [[1,0,0], [0,0,0], [0,0,0], [1,0,0], [0,1,0], [0,0,0], [1,1,0]] 
    # bc = [[1,0,0], [0,1,0], [0,0,0], [1,1,0]] 

    # Create models dir
    model_name = sys.argv[1]
    log_dir = "models/"
    os.makedirs(log_dir, exist_ok=True)
    model_path = os.path.join(log_dir, model_name)

    # Create and wrap the environment
    env = gym.make('SnekDriving-v0', body_config=bc, gui = True, max_num_simulation_steps = max_sim_steps_per_episode, terrain_type = terrain)

    # model = PPO("MlpPolicy", env, verbose=0)
    model = PPO.load(model_path, env)
    # model = DDPG(LnMlpPolicy, env, param_noise=param_noise, verbose=0)

    time_steps = num_episodes * max_sim_steps_per_episode

    # evaluate_policy(model, env)
    model.learn(total_timesteps=int(time_steps))

    print("------------SHOWCASE DONE")
