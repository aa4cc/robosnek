from setuptools import setup

setup(    
    name="snek_driving",
    version='0.0.1',
    install_requires=['gym', 'pybullet', 'numpy', 'matplotlib']
)
