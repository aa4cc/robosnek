import gym
import numpy as np
import pybullet as p
from snek_driving.resources.snek import Snek
from snek_driving.resources.plane import Plane
import matplotlib.pyplot as plt

class SnekDrivingEnv(gym.Env):
    metadata = {'render.modes': ['human']}  
  
    def __init__(self, body_config = [[1,0,0], [0,1,0], [1, 0, 0]], gui = False, do_urdf_generation = True, max_num_simulation_steps = 5000, terrain_type = "bumps"):
        print("INITIALIZING ENVIRONMENT WITH BODY CONFIG: ", body_config)
        self.client = None
        self.do_urdf_generation = do_urdf_generation
        self.body_config = body_config
        self.max_num_simulation_steps = max_num_simulation_steps
        self.num_restarts = 0
        self.num_envs = 1
        self.num_sim_steps_per_env_step = 5
        self.terrain_type = terrain_type

        if gui:
            self.client = p.connect(p.GUI)
        else:
            self.client = p.connect(p.DIRECT)

        self.snek = None
        self.rendered_img = None
        self.reset()
        # do not repeat urdf generation with same environment
        self.do_urdf_generation = False

        # self.action_space = gym.spaces.box.Box(low=np.array([0 for i in range(self.snek.num_action_dims)]), high=np.array([1 for i in range(self.snek.num_action_dims)]), dtype=np.float32)
        self.action_space = gym.spaces.box.Box(low=np.array([-1 for i in range(self.snek.num_action_dims)]), high=np.array([1 for i in range(self.snek.num_action_dims)]), dtype=np.float32)
        # self.action_space = gym.spaces.box.Box(
        #     low=np.array([0, -.6], dtype=np.float32),
        #     high=np.array([1, .6], dtype=np.float32))
        # self.action_space = gym.spaces.box.Box(low=0, high=1, shape=(1,self.snek.num_action_dims), dtype=np.float32)

        print("AS:")
        print(self.action_space)
        self.observation_space = gym.spaces.box.Box(
            low=np.array([-3.15 for i in range(self.snek.num_observation_dims)], dtype=np.float32),
            high=np.array([3.15 for i in range(self.snek.num_observation_dims)], dtype=np.float32))
        # print("AS sample ", self.action_space.sample())
        print("OS:")
        print(self.observation_space)

        # self.action_space = gym.spaces.box.Box(
        #     low=np.array([0, -.6], dtype=np.float32),
        #     high=np.array([1, .6], dtype=np.float32))
        # self.observation_space = gym.spaces.box.Box(
        #     low=np.array([-10, -10, -1, -1, -5, -5, -10, -10], dtype=np.float32),
        #     high=np.array([10, 10, 1, 1, 5, 5, 10, 10], dtype=np.float32))

        self.np_random, _ = gym.utils.seeding.np_random()

    def get_snek_directed_position(self):
        pos = p.getBasePositionAndOrientation(self.snek.car, self.client)
        # return pos[0][0] - np.power(pos[0][1], 2)
        # return pos[0][0]
        # return pos[0][0] + 2* pos[0][2]

        if self.terrain_type == "bumps": 
            return pos[0][0] - 0.5 * abs(pos[0][1])
        return pos[0][0] + 2* pos[0][2] - 0.5 * abs(pos[0][1])

    def step(self, action):
        if np.any(np.isnan(action)):
            action = np.ones(self.action_space.shape)
            print("NAN ACTION")

        self.snek.apply_action(action)

        for i in range(self.num_sim_steps_per_env_step):
            p.stepSimulation()
        obs = self.snek.get_observation()

        if np.any(np.isnan(obs)):
            print("NAN OBS")

        # compute reward
        # print("oriented pos: ", self.get_snek_directed_position())
        reward = 0
        oriented_pos = self.get_snek_directed_position()
        stepsize = 0.1
        # if abs(oriented_pos - self.reached_position_so_far) > stepsize:

        forward_rew = (oriented_pos - self.reached_position_so_far) * 100
        self.reached_position_so_far = oriented_pos
        # heading_rew = -np.power(obs[2], 2) * 1
        heading_rew = 0

        reward = forward_rew + heading_rew
        # print("HEADING REW: ", heading_rew)
        # print("FORWARD REW: ", forward_rew)

        # if oriented_pos > self.reached_position_so_far:
        #     reward = oriented_pos - self.reached_position_so_far
        #     self.reached_position_so_far = oriented_pos
        # elif oriented_pos < self.reached_position_so_far:
        #     reward = (oriented_pos - self.reached_position_so_far)
        #     self.reached_position_so_far = oriented_pos


        self.num_performed_steps += 1
        done = self.num_performed_steps >= self.max_num_simulation_steps
        if done:
            print("FINAL DIST: ", oriented_pos)

        if np.any(np.isnan(reward)):
            print("NAN REWARD")
            return 1/0
        # print(self.get_snek_directed_position())


        pos = p.getBasePositionAndOrientation(self.snek.car, self.client)
        if pos[0][2] < -2:
            done = True
            print("SNEK FELL")

        if pos[0][0] > 9.9:
            reward += 300
            done = True
            print("SNEK REACHED END")

            
        # print("ORI: ", obs[2])
        # print("REW:", reward)

        # print("ACT: ", action, " OBS:",  obs, "REW:", reward)
        return obs, reward, done, dict()

    def reset(self):
        # print("RESETTING ENV FOR TIME: ", self.num_restarts)
        self.num_restarts += 1
        p.resetSimulation(self.client)
        p.setGravity(0, 0, -10)

        # Reload the plane and snek
        self.snek = Snek(self.client, body_config = self.body_config, do_urdf_generation = self.do_urdf_generation, rotate_fully = (self.terrain_type=="bumps"))
        # print("SNEK SPAWNED")
        self.plane = Plane(self.client, terrain_type = self.terrain_type)
        # print("PLANE SPAWNED")
        self.reached_position_so_far = 0
        self.num_performed_steps = 0

        # do a few steps so that everything settles down
        # for i in range(10):
        #     self.step([0.5 for k in range(self.snek.num_action_dims)])

        self.reached_position_so_far = self.get_snek_directed_position()
        # print("RESET DONE")
        return self.snek.get_observation()

    def render(self, mode='human'):
        if self.rendered_img is None:
            self.rendered_img = plt.imshow(np.zeros((100, 100, 4)))

        # Base information
        car_id, client_id = self.snek.get_ids()
        # print("-pos:")
        # print(p.getBasePositionAndOrientation(car_id, client_id))
        # print("vel:")
        # print(p.getBaseVelocity(car_id, self.client))
        proj_matrix = p.computeProjectionMatrixFOV(fov=80, aspect=1,
                                                   nearVal=0.01, farVal=100)
        pos, ori = [list(l) for l in
                    p.getBasePositionAndOrientation(car_id, client_id)]
        print(pos)
        pos[0] = pos[0] - 1
        pos[1] = pos[1] - 0.2
        pos[2] = 0.2
        # pos[2] = 2

        # Rotate camera direction
        rot_mat = np.array(p.getMatrixFromQuaternion(ori)).reshape(3, 3)
        camera_vec = np.matmul(rot_mat, [1, 0, 0])
        up_vec = np.matmul(rot_mat, np.array([0, 0, 1]))
        view_matrix = p.computeViewMatrix(pos, pos + camera_vec, up_vec)

        # Display image
        frame = p.getCameraImage(100, 100, view_matrix, proj_matrix)[2]
        frame = np.reshape(frame, (100, 100, 4))
        self.rendered_img.set_data(frame)
        plt.draw()
        plt.pause(.00001)
        pass

    def close(self):
        p.disconnect(self.client)
        pass    

    def seed(self, seed=None): 
        self.np_random, seed = gym.utils.seeding.np_random(seed)
        return [seed]
