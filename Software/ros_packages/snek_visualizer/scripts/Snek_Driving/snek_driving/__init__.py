from gym.envs.registration import register

register(
    id='SnekDriving-v0', 
    entry_point='snek_driving.envs:SnekDrivingEnv'
)
