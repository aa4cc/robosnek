import pybullet as p
import os
import numpy as np


class Plane:
    def __init__(self, client, terrain_type="bumps"):
        spawn_rotation_euler = [0, 0, 2 * 3.1415 * (np.random.rand()- 0.5) ]
        # print("INITIAL ROTATION: ", spawn_rotation_euler[2])
        # if np.random.rand() > 0.5:
        #     spawn_rotation_euler[0] = 3.14159
        #     print("---FLIP!")
        spawn_rotation_quaternion = p.getQuaternionFromEuler(spawn_rotation_euler)

        if terrain_type == "plane":
            f_name = os.path.join(os.path.dirname(__file__), 'simpleplane.urdf')
            p.loadURDF(fileName=f_name,
                       basePosition=[0, 0, 0],
                       physicsClientId=client)
        elif terrain_type == "bumps":

            # f_name = os.path.join(os.path.dirname(__file__), 'bumps.urdf')
            f_name = os.path.join(os.path.dirname(__file__), 'bumps.urdf')
            p.loadURDF(fileName=f_name,
                       basePosition=[0, 0, 0],
                       baseOrientation=spawn_rotation_quaternion,
                       physicsClientId=client,
                       flags = p.GEOM_FORCE_CONCAVE_TRIMESH)

        elif terrain_type == "stairs":

            # f_name = os.path.join(os.path.dirname(__file__), 'bumps.urdf')
            f_name = os.path.join(os.path.dirname(__file__), 'stairs.urdf')
            p.loadURDF(fileName=f_name,
                       basePosition=[0, 0, 0],
                       # baseOrientation=spawn_rotation_quaternion,
                       physicsClientId=client,
                       flags = p.GEOM_FORCE_CONCAVE_TRIMESH)
