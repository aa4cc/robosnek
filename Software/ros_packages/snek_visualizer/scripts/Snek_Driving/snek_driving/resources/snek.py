import pybullet as p
import os
import math
import numpy as np
 
def euler_from_quaternion(x, y, z, w):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    roll is rotation around x in radians (counterclockwise)
    pitch is rotation around y in radians (counterclockwise)
    yaw is rotation around z in radians (counterclockwise)
    """
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    
    return roll_x, pitch_y, yaw_z # in radians

def getModuleName(body_config, idx):
    name = ""
    mod = body_config[idx]
    if mod[0] == 0:
        name += "bend_module"
    if mod[0] == 1:
        name += "wheel_module"
    if mod[0] == 2:
        name += "battery_module"
    name += "_n" + str(idx)
    return name

def getURDFstring(body_config, rootdir):
    f_header = open(rootdir + "header.xml", "r")
    f_head_module = open(rootdir + "head_module_template.xml", "r")
    f_end = open(rootdir + "end.xml", "r")
    str_header = f_header.read() + f_head_module.read()
    str_end = f_end.read()

    # obsv_data = []
    action_joint_ids = []

    total_string = str_header

    prev_module = [-1, 0, 0]
    # prev_linkname = "base_link"
    prev_mname = "base_link"
    prev_mtype  = -1

    for i in range(len(body_config)):
        module = body_config[i]

        module_type = module[0]
        module_rot1 = module[1]
        module_rot2 = module[2]
        # print("ADDING MODULE TYPE %d", module_type)
        mname = getModuleName(body_config, i)

        # ADD MODULE, WITH FRONT AND ASS AND JOINT BETWEEN THOSE (AND ANYTHING ELSE)
        if module_type == 2:
            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "battery_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr
        if module_type == 1:

            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "wheel_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr
        if module_type == 0:

            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "bend_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr

        # DECIDE HOW TO CONNECT THIS MODULE TO PREVIOUS (FRONT-FRONT, ASS-FRONT, ASS-ASS)
        if module_type == 1:
            prev_linkname = mname
            #TODO self.static_transforms.append()

        # ADD JOINT TO PREVIOUS MODULE
        f_joint = open(rootdir + "fixed_joint_template.xml", "r")
        jstr = f_joint.read()
        f_joint.close()

        jstr = jstr.replace("NAMEHERE", "joint_n" + str(i))

        jx, jy, jz = 0.0, 0.0, 0.0
        r1, r2, r3 = 0.0, 0.0, 0.0
        r1 = module_rot1 * math.pi/2
        jstr = jstr.replace("XHERE", str(jx))
        jstr = jstr.replace("YHERE", str(jy))
        jstr = jstr.replace("ZHERE", str(jz))

        jstr = jstr.replace("R1HERE", str(r1))
        jstr = jstr.replace("R2HERE", str(r2))
        jstr = jstr.replace("R3HERE", str(r3))

        jstr = jstr.replace("PARENTHERE", prev_mname + "_back")
        jstr = jstr.replace("CHILDHERE", mname)
        total_string += jstr

        prev_mname = mname

    total_string += str_end
    return total_string, action_joint_ids

def generateURDF(body_config, rootdir):
    # f_target = open(rootdir + "test.xml", "w")
    f_name = os.path.join(os.path.dirname(__file__), 'snek.urdf')
    print("WRITING URDF TO ", f_name)
    f_target = open(f_name, "w")

    urdfstring, action_joint_ids = getURDFstring(body_config, rootdir)
    f_target.write(urdfstring)
    f_target.close()
    print("URDF GENERATION SUCCESSFUL")
    return action_joint_ids

class Snek:
    def __init__(self, client, body_config, do_urdf_generation=False, rotate_fully = False):
        self.client = client

        self.max_bend_force = 1
        self.max_bend_angle = (3.14 / 2) * (80. / 90.)
        self.max_wheel_velocity = 3.14 * 3

        # CREATE URDF FILE
        # urdf_path = "/home/tom/snek/robosnek/Software/ros_packages/snek_visualizer/urdf_files/"
        urdf_path = os.path.join(os.path.dirname(__file__), 'urdf_files/')
        if do_urdf_generation:
            self.action_joint_ids = generateURDF(body_config, urdf_path)
        else:
            trash, self.action_joint_ids = getURDFstring(body_config, urdf_path)

        f_name = os.path.join(os.path.dirname(__file__), 'snek.urdf')

        # spawn_rotation_euler = [np.random.rand() * 3.14159 * 2, 0, np.random.rand() * 3.14159 * 2]
        # spawn_rotation_euler = [0, 0, np.random.rand() * 3.14159 * 2]
        # spawn_rotation_euler = [0, 0, 2 * 3.1415 * (np.random.rand()- 0.5) ]
        spawn_rotation_euler = [0, 0, 1 * 3.1415 * (np.random.rand()- 0.5) ]
        if rotate_fully:
            spawn_rotation_euler = [0, 0, 2 * 3.1415 * (np.random.rand()- 0.5) ]
        # print("INITIAL ROTATION: ", spawn_rotation_euler[2])
        # if np.random.rand() > 0.5:
        #     spawn_rotation_euler[0] = 3.14159
        #     print("---FLIP!")
        spawn_rotation_quaternion = p.getQuaternionFromEuler(spawn_rotation_euler)

        self.car = p.loadURDF(fileName=f_name,
                              basePosition=[0, 0, 0.20],
                              # basePosition=[0, 0, 1.2],
                              baseOrientation = spawn_rotation_quaternion ,
                              physicsClientId=client)


        # FIND JOINT IDS
        # print("SNEK NUM JOINTS: ", p.getNumJoints(self.car, self.client))
        self.wheel_joint_idxs = []
        self.bend_joint_idxs = []
        tp = 1
        for i in range(p.getNumJoints(self.car, self.client)):
            j = p.getJointInfo(self.car, i, self.client)
            # print(j)
            jname = str(j[1])
            # print(jname)
            if "action" in jname:
                if "wheel" in jname:
                    # print("WHEEL FOUND")
                    self.wheel_joint_idxs.append(i)
                    # p.setJointMotorControl2(self.car, i, p.VELOCITY_CONTROL, targetVelocity=tp*self.max_wheel_velocity)
                    # tp *= -1
                elif "bend" in jname:
                    # print("BEND FOUND")
                    self.bend_joint_idxs.append(i)
                    # p.setJointMotorControl2(self.car, i, p.POSITION_CONTROL, targetPosition=-3.14 / 4, force = self.max_bend_force)
        # print("NUM ACTION DIMS: ", )

        self.num_action_dims = len(self.bend_joint_idxs) + len(self.wheel_joint_idxs)
        # self.num_observation_dims = len(self.bend_joint_idxs) + 3
        self.num_observation_dims = len(self.bend_joint_idxs) + 3

    def get_ids(self):
        return self.car, self.client

    def apply_action(self, action):
        # expects all actions to be 0 to 1
        # first actions are wheel, the rest are bend
        num_wheels = len(self.wheel_joint_idxs)
        num_bend = len(self.bend_joint_idxs)
        # wheel_velocities = [self.max_wheel_velocity * (1 - 2.0 * a) for a in action[0:num_wheels]]
        # bend_angles = [self.max_bend_angle * (1 - 2.0 * a) for a in action[num_wheels:(num_wheels+num_bend)]]

        wheel_velocities = [self.max_wheel_velocity * a for a in action[0:num_wheels]]
        bend_angles = [self.max_bend_angle * a for a in action[num_wheels:(num_wheels+num_bend)]]

        # self.max_bend_velocity = 3.1415
        # bend_angles = [self.max_bend_velocity  * a for a in action[num_wheels:(num_wheels+num_bend)]]

        for i in range(num_wheels):
            p.setJointMotorControl2(self.car, self.wheel_joint_idxs[i], p.VELOCITY_CONTROL, targetVelocity=wheel_velocities[i])

        for i in range(num_bend):
            p.setJointMotorControl2(self.car, self.bend_joint_idxs[i], p.POSITION_CONTROL, targetPosition=bend_angles[i], force = self.max_bend_force)
            # p.setJointMotorControl2(self.car, self.bend_joint_idxs[i], p.VELOCITY_CONTROL, targetVelocity=bend_angles[i], force = self.max_bend_force)


        # # Expects action to be two dimensional
        # throttle, steering_angle = action

        # # Clip throttle and steering angle to reasonable values
        # throttle = min(max(throttle, 0), 1)
        # steering_angle = max(min(steering_angle, 0.6), -0.6)

        # # Set the steering joint positions
        # p.setJointMotorControlArray(self.car, self.steering_joints,
        #                             controlMode=p.POSITION_CONTROL,
        #                             targetPositions=[steering_angle] * 2,
        #                             physicsClientId=self.client)

        # # Calculate drag / mechanical resistance ourselves
        # # Using velocity control, as torque control requires precise models
        # friction = -self.joint_speed * (self.joint_speed * self.c_drag +
        #                                 self.c_rolling)
        # acceleration = self.c_throttle * throttle + friction
        # # Each time step is 1/240 of a second
        # self.joint_speed = self.joint_speed + 1/30 * acceleration
        # if self.joint_speed < 0:
        #     self.joint_speed = 0

        # # Set the velocity of the wheel joints directly
        # p.setJointMotorControlArray(
        #     bodyUniqueId=self.car,
        #     jointIndices=self.drive_joints,
        #     controlMode=p.VELOCITY_CONTROL,
        #     targetVelocities=[self.joint_speed] * 4,
        #     forces=[1.2] * 4,
        #     physicsClientId=self.client)

    def get_observation(self):
        base_link_id = 0
        base_state = p.getLinkState(self.car, base_link_id)
        # print("BASE STATE: POS: ", base_state[0], " ORI: ", base_state[1])
        roll, pitch, yaw = euler_from_quaternion(base_state[1][0], base_state[1][1], base_state[1][2] ,base_state[1][3])

        # print("RPY: ", roll, "," , pitch, ",", yaw)
        # GIVE ROLL AND PITCH WHICH ARE BOUND TO -pi/2, pi/2

        # obsv = [roll, pitch]
        obsv = np.zeros(self.num_observation_dims, dtype = np.float32)
        obsv[0] = abs(roll)
        obsv[1] = pitch
        obsv[2] = yaw
        # bend_joint_states = p.getJointStates(self.car, self.bend_joint_idxs)[1]
        # print(bend_joint_states)
        should_clamp = True

        for i in range(len(self.bend_joint_idxs)):
            bend_joint_state = p.getJointState(self.car, self.bend_joint_idxs[i])[0]
            # print(bend_joint_state)
            # obsv.append(bend_joint_state)
            obsv[3 + i] = bend_joint_state
            # if should_clamp:

        # print(base_state)
        # base_quaternion = 
        # base_quaternion = 
        # return (0, 0, 0, 0, 0)
        return obsv

#         # Get the position and orientation of the car in the simulation
#         pos, ang = p.getBasePositionAndOrientation(self.car, self.client)
#         ang = p.getEulerFromQuaternion(ang)
#         ori = (math.cos(ang[2]), math.sin(ang[2]))
#         pos = pos[:2]
#         # Get the velocity of the car
#         vel = p.getBaseVelocity(self.car, self.client)[0][0:2]

#         # Concatenate position, orientation, velocity
#         observation = (pos + ori + vel)

#         return observation
