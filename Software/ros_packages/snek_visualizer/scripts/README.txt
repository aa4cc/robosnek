HOW TO INSTALL:
1) install python libraries: gym, pybullet, numpy, stable_baselines3, matplotlib and any others if python requires it during runtime
2) install the Snek_Driving AI Gym environment by going into folder Snek_Driving (where setup.py is) and running "pip install -e . "

HOW TO USE:
-to train a model, run train_and_generate_results.py. By default, this will save a model in the models/ folder every 5 episodes.
-the script train_and_generate_results.py also generates a graph of episode rewards at the end of training
-training parameters (num episodes, episode length, robot configuration) can be changed inside the script
-to showcase the model, run the script showcase_model.py modelname without the directory or .zip extension 
  (the script automatically looks for that model zip file in the models directory). For example "python3 showcase_model.py latest_model_ep_100"
  and dont forget to set the learning terrain and robot configuration in showcase_model.py the same as in train_and_generate_results.py!


The environment was created thanks to this guide - https://gerardmaggiolino.medium.com/creating-openai-gym-environments-with-pybullet-part-1-13895a622b24

