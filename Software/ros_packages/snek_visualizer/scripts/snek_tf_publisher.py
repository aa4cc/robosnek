#! /usr/bin/env python

from tf import TransformBroadcaster
# import transformations.py
import rospy
from rospy import Time 

from snek_core.common_utils import *
from snek_msgs.msg import *
# from std_srvs.srv import Trigger

def getModuleName(body_config, idx):
    name = ""
    mod = body_config[idx]
    if mod[0] == 0:
        name += "bend_module"
    if mod[0] == 1:
        name += "wheel_module"
    if mod[0] == 2:
        name += "battery_module"
    name += "_n" + str(idx)
    return name

def getURDFstring(body_config, rootdir):
    f_header = open(rootdir + "header.xml", "r")
    f_head_module = open(rootdir + "head_module_template.xml", "r")
    f_end = open(rootdir + "end.xml", "r")
    str_header = f_header.read() + f_head_module.read()
    str_end = f_end.read()

    total_string = str_header

    prev_module = [-1, 0, 0]
    # prev_linkname = "base_link"
    prev_mname = "base_link"
    prev_mtype  = -1

    for i in range(len(body_config)):
        module = body_config[i]

        module_type = module[0]
        module_rot1 = module[1]
        module_rot2 = module[2]
        print("ADDING MODULE TYPE %d", module_type)
        mname = getModuleName(body_config, i)

        # ADD MODULE, WITH FRONT AND ASS AND JOINT BETWEEN THOSE (AND ANYTHING ELSE)
        if module_type == 2:
            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "battery_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr
        if module_type == 1:

            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "wheel_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr
        if module_type == 0:

            mname = getModuleName(body_config, i)
            f_module = open(rootdir + "bend_module_template.xml", "r")
            mstr = f_module.read()
            f_module.close()

            mstr  = mstr.replace("NAMEHERE", mname)
            total_string += mstr

        # DECIDE HOW TO CONNECT THIS MODULE TO PREVIOUS (FRONT-FRONT, ASS-FRONT, ASS-ASS)
        if module_type == 1:
            prev_linkname = mname
            #TODO self.static_transforms.append()

        # ADD JOINT TO PREVIOUS MODULE
        f_joint = open(rootdir + "fixed_joint_template.xml", "r")
        jstr = f_joint.read()
        f_joint.close()

        jstr = jstr.replace("NAMEHERE", "joint_n" + str(i))

        jx, jy, jz = 0.0, 0.0, 0.0
        r1, r2, r3 = 0.0, 0.0, 0.0
        jstr = jstr.replace("XHERE", str(jx))
        jstr = jstr.replace("YHERE", str(jy))
        jstr = jstr.replace("ZHERE", str(jz))

        jstr = jstr.replace("R1HERE", str(r1))
        jstr = jstr.replace("R2HERE", str(r2))
        jstr = jstr.replace("R3HERE", str(r3))

        jstr = jstr.replace("PARENTHERE", prev_mname + "_back")
        jstr = jstr.replace("CHILDHERE", mname)
        total_string += jstr

        prev_mname = mname

    total_string += str_end
    return total_string

def generateURDF(body_config, rootdir):
    rospy.loginfo("WRITING URDF TO %s", rootdir)
    f_target = open(rootdir + "test.xml", "w")
    f_target.write(getURDFstring(body_config, rootdir))
    f_target.close()
    print("URDF GENERATION SUCCESSFUL")
    

class SnekTFPublisher(object):

# # #{ INIT
    def __init__(self):
        # self.control_freq = rospy.get_param('~control_freq', 10.0) # control loop frequency (Hz)
        # self.body_config = rospy.get_param('~body_config', [])
        self.body_config = rospy.get_param('~body_config', [[0, 0, 0], [1,0,0]])

        self.tf_broadcaster = TransformBroadcaster()
        # self.body_config = []

        # if len(self.body_config) == 0:
        #     self.sub_body_config = rospy.Subscriber('body_config', BodyConfigMsg, self.handle_BodyConfigMsg)
        #     waitrate = rospy.Rate(10)
        #     while len(self.body_config) == 0:
        #         waitrate.sleep()
        #     self.sub_body_config.unregister()

            

        rospy.loginfo('Snek TF publisher initialized.')
        self.URDFpath = rospy.get_param('~snek_urdf_path', "empty!")
        self.generateURDF(self.body_config, self.URDFpath )

        # self.mainloop()
# # #}

    def handle_BodyConfigMsg(self, msg):
        self.body_config = []
        n_modules = len(msg.data) // 3
        print("BC:", msg.data, " GOT ",n_modules, " MODULES" )
        for i in range(n_modules):
            self.body_config.append([msg.data[i*3], msg.data[i*3+1], msg.data[i*3+2]])
        rospy.loginfo('BODY CONFIG MSG RECEIVED:')
        print(self.body_config)

    def getModuleName(self, body_config, idx):
        return getModuleName(body_config, idx)


    def generateURDF(self, body_config, rootdir):
        generateURDF(body_config, rootdir)

    def publishStaticTransforms(self):
        pass

if __name__ == '__main__':
    rospy.init_node('snek_tf_publisher', log_level=rospy.INFO)
    node = SnekTFPublisher()
    rospy.shutdown()
