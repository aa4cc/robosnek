import gym
import torch
# from agent import TRPOAgent
# import pybullet_envs
import snek_driving
from stable_baselines3.common.evaluation import evaluate_policy

import time
import timeit

from stable_baselines3 import PPO
from stable_baselines3 import A2C
from stable_baselines3 import DDPG
from stable_baselines3 import SAC

from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.vec_env import DummyVecEnv, VecNormalize

# from agent import TRPOAgent
# start = timeit.timeit()
# print("hello")
# end = timeit.timeit()
# print(end - start)


def main():
    # nn = torch.nn.Sequential(torch.nn.Linear(8, 64), torch.nn.Tanh(),
    #                          torch.nn.Linear(64, 2))
    # agent = TRPOAgent(policy=nn)

    # agent.load_model("agent.pth")
    # agent.train("SnekDriving-v0", seed=0, batch_size=5000, iterations=100,
    #             max_episode_length=250, verbose=True)
    # agent.save_model("agent.pth")

    # env = gym.make('SnekDriving-v0')
    # env = make_vec_env("SnekDriving-v0", n_envs=2)
    
    # model = A2C("MlpPolicy", env, verbose=1)
    # model.learn(total_timesteps=25000)


    # env = gym.make('CartPole-v1')
    # model = PPO("MlpPolicy", env, verbose=1)
    # model.learn(total_timesteps=25000)

    # bc = [[1,0,0], [0,0,0], [0,0,0], [1, 0, 0],[0,0,0], [0,0,0], [1, 0, 0]]
    # bc = [[1,0,0], [0,1,0], [1,0,0]]

    # SUPERCAR
    bc = [[1,0,0], [0,1,0], [0,0,0], [1,1,0]]
    # SNEK 1
    # bc = [[1,0,0], [0,0,0], [0,0,0], [1,0,0], [0,1,0], [0,0,0], [1,1,0]]

    env = gym.make('SnekDriving-v0', body_config=bc, gui = True)
    # env = gym.make('AntBulletEnv-v0')
    # env = make_vec_env("AntBulletEnv-v0", n_envs=4)
    # env = VecNormalize(env, norm_obs=True, norm_reward=False, clip_obs=10.)

    # env = make_vec_env("CartPole-v1", n_envs=4)
    # env = gym.make('MountainCarContinuous-v0')

    model = A2C("MlpPolicy", env, verbose=1)
    # model = PPO("MlpPolicy", env, verbose=1)
    # model = A2C("MlpPolicy", env, verbose=1)
    # model = A2C(policy = "MlpPolicy",
    #         env = env,
    #         gae_lambda = 0.9,
    #         gamma = 0.99,
    #         learning_rate = 0.00096,
    #         max_grad_norm = 0.5,
    #         n_steps = 8,
    #         vf_coef = 0.4,
    #         ent_coef = 0.0,
    #         tensorboard_log = "./tensorboard",
    #         policy_kwargs=dict(
    #         log_std_init=-2, ortho_init=False),
    #         normalize_advantage=False,
    #         use_rms_prop= True,
    #         use_sde= True,
    #         verbose=1)

    # model = DDPG("MlpPolicy", env, verbose=1)
    # model.learn(total_timesteps = 2500)
    # model.learn(total_timesteps = 30000)
    model.learn(total_timesteps = 1000 * 5000)

    # env = gym.make('SnekDriving-v0', body_config=bc, gui = True)
    # model.learn(total_timesteps = 30000000000)

    print("------------LEARNING DONE")

    evaluate_policy(model, env)

    print("------------EVAL DONE")

    # ob = env.reset()
    # start = timeit.timeit()
    # num_iters = 0
    # done = False
    # # while True:
    # while not done:
    #     # action = agent(ob)
    #     # ob, _, done, _ = env.step(action)
    #     # ob, _, done, _ = env.step(None)
    #     # env.step(None)
    #     # env.step(env.action_space.sample())
    #     ob, rew, done, _ = env.step(env.action_space.sample())
    #     # print("OBS:", ob)
    #     # if(num_iters % 20 == 0):
    #     #     env.render()
    #     end = timeit.timeit()
    #     dt = (end - start)
    #     # print("delta time:" , dt, "iters:", num_iters)
    #     # if done:
    #     #     ob = env.reset()
    #     # time.sleep(1.0/240.0)
    #     num_iters += 1


if __name__ == '__main__':
    main()
