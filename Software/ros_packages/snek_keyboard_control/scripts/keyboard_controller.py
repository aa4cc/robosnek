import time
import serial
import rospy
import sys
# import keyboard
from pynput import keyboard

# from snek_core.common_utils import say_it_works
from snek_core.common_utils import *
from snek_msgs.srv import *
from snek_msgs.msg import *
from std_srvs.srv import Trigger

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    print('{0} released'.format(
        key))
    if key == keyboard.Key.esc:
        # Stop listener
        return False

class KeyboardController(object):

# # #{ INIT
    def __init__(self):
        # self.control_freq = rospy.get_param('~control_freq', 10.0) # control loop frequency (Hz)
        # self.body_config = rospy.get_param('~body_config', [[0,0,0], [1,0,0]])
        self.body_config = []

        if len(self.body_config) == 0:
            self.sub_body_config = rospy.Subscriber('body_config', BodyConfigMsg, self.handle_BodyConfigMsg)
            waitrate = rospy.Rate(10)
            while len(self.body_config) == 0:
                waitrate.sleep()
            self.sub_body_config.unregister()

            

        rospy.loginfo('Keyboard controller initialized.')
        self.keyboard_reading_freq = 100
        self.keyboard_reading_rate = rospy.Rate(self.keyboard_reading_freq)
        self.should_stop = False
        self.is_pressed_w = False
        self.key_press_cache = {}

        self.internal_setpoints = []

        # WAIT FOR CORE AND GET BODY CONFIG

        self.bangbang_bendies_angle = 90.0
        self.bangbang_wheels_speed = 0

        self.selected_bendy = 0

        # self.mode = "BB"
        self.mode_button_pressed = False
        self.mode = "C"

        # INIT DEFAULT SETPOINT ARRAY MSG AND SEND IT
        setpoints_to_send  = self.generate_setpoints_for_all_modules()

        # SEND DEFAULT CONFIG
        self.publish_setpoints(setpoints_to_send)

        self.mainloop()
# # #}

    def handle_BodyConfigMsg(self, msg):
        self.body_config = []
        n_modules = len(msg.data) // 3
        print("BC:", msg.data, " GOT ",n_modules, " MODULES" )
        num_bendies = 0
        self.bendy_angles = []
        for i in range(n_modules):
            self.body_config.append([msg.data[i*3], msg.data[i*3+1], msg.data[i*3+2]])
            if msg.data[i*3] == 0:
                num_bendies += 1
                self.bendy_angles.append(90.0)
        print("BA:")
        print(self.bendy_angles)

        rospy.loginfo('BODY CONFIG MSG RECEIVED:')
        print(self.body_config)

# # #{ KEY PRESS HANDLING
    def is_pressed(self, c):
        if c in self.key_press_cache:
            return self.key_press_cache[c]
        return False
    
    def on_press(self, key):
        try:
            # print('alphanumeric key {0} pressed'.format(
                # key.char))
            self.key_press_cache [key.char] = True
        except AttributeError:
            print('special key {0} pressed'.format(
                key))
    
    def on_release(self, key):
        try:
            self.key_press_cache [key.char] = False
            if key.char == "q":
                # Stop listener
                print("setting stop true")
                self.should_stop = True
                return False
        except AttributeError:
            print('special key {0} released'.format(
                key))

# # #}

    def publish_setpoints(self, msgs_to_send, blocked_msgs = []):
        msg_arr = ModuleSetpointArray()
        if len(msgs_to_send) > 0:
            msg_arr.msgs = msgs_to_send
            self.cmd_pub = rospy.Publisher('module_setpoints', ModuleSetpointArray, queue_size=20) # Publisher of velocity commands
            self.cmd_pub.publish(msg_arr)
        self.last_sent_setpoints = msgs_to_send + blocked_msgs
    
    def update_internal_values_based_on_input(self):
        anglestep = 45.0
        anglestep2 = 30.0
        anglestep3 = 50.0
        cont_anglestep = 0.5
        wheelstep = 1

        if self.is_pressed("m"):
            self.mode_button_pressed = True
        else:
            if self.mode_button_pressed:
                # if self.mode == "BB":
                #     self.mode = "C"
                if self.mode == "C":
                    self.mode = "T"
                elif self.mode == "T":
                    self.mode = "C"
                rospy.loginfo("MODE CHANGED TO %s!", self.mode)
            self.mode_button_pressed = False


        if self.mode == "BB":
            if self.is_pressed("a") and not self.is_pressed("d"):
                self.bangbang_bendies_angle = 90.0 - anglestep
            elif self.is_pressed("d") and not self.is_pressed("a"):
                self.bangbang_bendies_angle = 90.0 + anglestep
            else:
                self.bangbang_bendies_angle = 90.0


            if self.is_pressed("w") and not self.is_pressed("s"):
                self.bangbang_wheels_speed = wheelstep
            elif self.is_pressed("s") and not self.is_pressed("w"):
                self.bangbang_wheels_speed  = -wheelstep
            else:
                self.bangbang_wheels_speed  = 0

            if self.is_pressed("j") and not self.is_pressed("k"):
                self.bendy_angles[1] = 90 - anglestep2
            elif self.is_pressed("k") and not self.is_pressed("j"):
                self.bendy_angles[1] = 90 + anglestep2
            else:
                self.bendy_angles[1] = 90

        if self.mode == "C":
            if self.is_pressed("a") and not self.is_pressed("d"):
                # self.bendy_angles[0] += cont_anglestep 
                self.bendy_angles[0] = 90 - anglestep
            elif self.is_pressed("d") and not self.is_pressed("a"):
                self.bendy_angles[0] = 90 + anglestep
                # self.bendy_angles[0] -= cont_anglestep 
            else:
                self.bendy_angles[0] = 90

            if self.is_pressed("j") and not self.is_pressed("k"):
                self.bendy_angles[1] = 90 - anglestep2
            elif self.is_pressed("k") and not self.is_pressed("j"):
                self.bendy_angles[1] = 90 + anglestep2
            elif self.is_pressed("u") and not self.is_pressed("i"):
                self.bendy_angles[1] = 90 - anglestep3
            elif self.is_pressed("i") and not self.is_pressed("u"):
                self.bendy_angles[1] = 90 + anglestep3
            else:
                self.bendy_angles[1] = 90



            # if self.is_pressed("j") and not self.is_pressed("k"):
            #     self.bendy_angles[1] += cont_anglestep 
            # elif self.is_pressed("k") and not self.is_pressed("j"):
            #     self.bendy_angles[1] -= cont_anglestep 

            if self.is_pressed("w") and not self.is_pressed("s"):
                self.bangbang_wheels_speed = wheelstep
            elif self.is_pressed("s") and not self.is_pressed("w"):
                self.bangbang_wheels_speed  = -wheelstep
            else:
                self.bangbang_wheels_speed  = 0

        # CLAMP IF IN ANGLE CONTROL
        for i in range(len(self.bendy_angles)):
            if self.bendy_angles[i] > 180:
                self.bendy_angles[i] = 180
            if self.bendy_angles[i] < 0:
                self.bendy_angles[i] = 0

        torquestep = 3
        if self.mode == "T":
            if self.is_pressed("a") and not self.is_pressed("d"):
                print("setting negative torquestep")
                self.bendy_angles[0] = -torquestep
            elif self.is_pressed("d") and not self.is_pressed("a"):
                self.bendy_angles[0] = torquestep
            else:
                self.bendy_angles[0] = 0

            if self.is_pressed("j") and not self.is_pressed("k"):
                self.bendy_angles[1] = -torquestep
            elif self.is_pressed("k") and not self.is_pressed("j"):
                self.bendy_angles[1] = torquestep
            else:
                self.bendy_angles[1] = 0

            # if self.is_pressed("j") and not self.is_pressed("k"):
            #     self.bendy_angles[1] += cont_anglestep 
            # elif self.is_pressed("k") and not self.is_pressed("j"):
            #     self.bendy_angles[1] -= cont_anglestep 

            if self.is_pressed("w") and not self.is_pressed("s"):
                self.bangbang_wheels_speed = wheelstep
            elif self.is_pressed("s") and not self.is_pressed("w"):
                self.bangbang_wheels_speed  = -wheelstep
            else:
                self.bangbang_wheels_speed  = 0

        # # ESTOP
        # if self.is_pressed("e"):


    def generate_setpoints_for_all_modules(self):
        setpoints = []
        gen_time = rospy.Time()
        bend_index = 0
        for i in range(len(self.body_config)):
            # rospy.loginfo("MODULE %d", i)
            msg = ModuleSetpoint()
            msg.header.stamp = gen_time
            msg.module_address = i + 1

            if self.mode == "BB":
                if self.body_config[i][0] == 0:
                    rospy.loginfo_once("BENDY MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 0
                    msg.setpoint = self.bangbang_bendies_angle
                elif self.body_config[i][0] == 1:
                    rospy.loginfo_once("WHEEL MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 1
                    msg.setpoint = self.bangbang_wheels_speed
                else:
                    rospy.logwarn_once("UNRECOGNIZED MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 0
                setpoints.append(msg)

            elif self.mode == "C":
                if self.body_config[i][0] == 0:
                    rospy.loginfo_once("BENDY MODULE AT ADDR %d, BI:%d", msg.module_address, bend_index)
                    msg.control_type = 0
                    msg.setpoint = self.bendy_angles[bend_index]
                    rospy.loginfo("SETTING %f DEG TO BENDY n %d", msg.setpoint, bend_index)
                    bend_index += 1
                elif self.body_config[i][0] == 1:
                    rospy.loginfo_once("WHEEL MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 1
                    msg.setpoint = self.bangbang_wheels_speed
                else:
                    rospy.logwarn_once("UNRECOGNIZED MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 0
                setpoints.append(msg)

            elif self.mode == "T":
                if self.body_config[i][0] == 0:
                    rospy.loginfo_once("BENDY MODULE AT ADDR %d, BI:%d", msg.module_address, bend_index)
                    msg.control_type = 2
                    msg.setpoint = self.bendy_angles[bend_index]
                    rospy.loginfo("SETTING %f TORQUE TO BENDY n %d", self.bendy_angles[bend_index], bend_index)
                    bend_index += 1
                elif self.body_config[i][0] == 1:
                    rospy.loginfo_once("WHEEL MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 1
                    msg.setpoint = self.bangbang_wheels_speed
                else:
                    rospy.logwarn_once("UNRECOGNIZED MODULE AT ADDR %d", msg.module_address)
                    msg.control_type = 0
                setpoints.append(msg)
        return setpoints

    def get_nonredundant_setpoint_msgs(self, current_setpoints, last_sent_setpoints):
        setpoints_to_send = []
        blocked_msgs = []
        for msg in current_setpoints:
            is_redudnant = False
            for msg2 in last_sent_setpoints:
                if msg2.module_address != msg.module_address:
                    continue
                if msg2.control_type != msg.control_type:
                    continue
                if abs(msg2.setpoint - msg.setpoint) > 0.00001:
                    continue
                is_redudnant = True
                blocked_msgs.append(msg2)
                break
            if not is_redudnant:
                setpoints_to_send.append(msg)
        return setpoints_to_send, blocked_msgs

    def mainloop(self):
        # NONBLOCKING LISTENER
        listener = keyboard.Listener(
            on_press=self.on_press,
            on_release=self.on_release)
        listener.start()

        # LOOP
        while not self.should_stop:
            # print(self.is_pressed("w"), self.is_pressed("a"))
            self.update_internal_values_based_on_input()
            orig_msgs = self.generate_setpoints_for_all_modules()
            reduced_msgs, blocked_msgs = self.get_nonredundant_setpoint_msgs(orig_msgs, self.last_sent_setpoints)
            self.publish_setpoints(reduced_msgs, blocked_msgs)
            # if len(reduced_msgs) > 0:
            #     rospy.loginfo("SENDING %d MSGS, REDUCED FROM %d COMMANDS", len(reduced_msgs), len(orig_msgs))

            self.keyboard_reading_rate.sleep()
        rospy.loginfo("exiting keyboard control")


if __name__ == '__main__':
    rospy.init_node('keyboard_controller', log_level=rospy.INFO)
    node = KeyboardController()


