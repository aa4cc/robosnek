import rospy
import math
# from snek_msgs.msg import *
from snek_msgs.msg import *
from enum import Enum, auto

# # #{ MSG AND MODULE ENUM DEFINITIONS
class msgtype(Enum):
    invalid_message = auto()
    
    read_encoder_angles = auto()
    read_current = auto()

    set_angle_setpoint = auto()
    set_angular_velocity_setpoint = auto()
    set_torque_setpoint = auto()

    set_PID_constants = auto()
    disable_motors = auto()
    initialize_addresses = auto()

    corrupted_msg = auto()
msgdict = {}
msgdict[msgtype.invalid_message] = "invalid message"
msgdict[msgtype.read_encoder_angles] = "read encoder angles"
msgdict[msgtype.read_current] = "read current"
msgdict[msgtype.set_angle_setpoint] = "angle setpoint"
msgdict[msgtype.set_angular_velocity_setpoint] = "velocity setpoint"
msgdict[msgtype.set_torque_setpoint] = "torque setpoint"
msgdict[msgtype.set_PID_constants] = "set PID consts"
msgdict[msgtype.disable_motors] = "disable motors"
msgdict[msgtype.initialize_addresses] = "init addr"

MAX_CURRENT = 0.255 #[A]
# MAX_BEND_MODULE_ANGLE = math.pi 
MAX_BEND_MODULE_ANGLE = 2*180.0
MIN_BEND_MODULE_ANGLE = 0 

class moduletype(Enum):
    bendy_module_1axis = auto()
    wheel_module = auto()
# # #}

def bytearr_to_intstr(bytearr):
    bytearr_string = ""
    for b in bytearr:
        bytearr_string = bytearr_string + "," + str(int(b))
    return bytearr_string + ","

# # #{ SERIAL CONVERSION DEFINITIONS
def parse_serial_msg_data(bytearr, request_msg_type = None):
    # print("ATTEMPTING TO PARSE ", bytearr)

    rospy.loginfo("ATTEMPTING TO PARSE %s", bytearr_to_intstr(bytearr))

    try:

        #TODO Fix spetial case of addressing, not sending onebyte (of sender ID)
        if request_msg_type == msgtype.initialize_addresses:
            return msgtype.initialize_addresses, 0, [bytearr[-1]]

        datalen = len(bytearr)
        sender_id = bytearr[2]

        if request_msg_type == msgtype.read_encoder_angles:
            # NOW MAP RECEIVED ENC VALUES TO ANGLES!!!!
            rospy.loginfo("ENCODERS RECEVIED VAL: %d, %d" , bytearr[4], bytearr[5])
            angleval1 = MAX_BEND_MODULE_ANGLE * int(bytearr[4]) / 255.0 + MIN_BEND_MODULE_ANGLE
            angleval2 = MAX_BEND_MODULE_ANGLE * int(bytearr[5]) / 255.0 + MIN_BEND_MODULE_ANGLE

            return msgtype.read_encoder_angles, sender_id, [angleval1, angleval2]

        if request_msg_type == msgtype.read_current:
            # MAP CURRENT TO MAX CURRENT
            currentval = MAX_CURRENT * bytearr[4] / 255.0

            return msgtype.read_encoder_angles, sender_id, [currentval]

        if request_msg_type == msgtype.disable_motors:
            if bytearr[0] == 255 and bytearr[1] == 1 and bytearr[2] == 254:
                return msgtype.disable_motors, sender_id, []

    except Exception as e:
        rospy.logwarn("SERIAL MSG PARSING WENT WRONG! EXCEPTION:")
        print(e)
    
    rospy.logwarn("INVALID MSG RECEIVED!")
    return msgtype.invalid_message, 0, 0

def construct_serial_msg(mtype, module_id = None, msgdata = None):
    if mtype == msgtype.read_encoder_angles:
        return module_id.to_bytes(1, 'big') + b'\x01\x00'

    if mtype == msgtype.read_current:
        return module_id.to_bytes(1, 'big') + b'\x01\x01'

    if mtype == msgtype.set_angle_setpoint:
        goal_angle = msgdata[0]
        if goal_angle > MAX_BEND_MODULE_ANGLE or goal_angle < MIN_BEND_MODULE_ANGLE:
            rospy.logwarn("REQUESTED ANGLE SETPOINT IS OUTSIDE OF BOUNDS!")
        setpoint = int(255 * (goal_angle - MIN_BEND_MODULE_ANGLE) / (MAX_BEND_MODULE_ANGLE - MIN_BEND_MODULE_ANGLE))
        rospy.loginfo("ENCODED SETPOINT BYTEVAL: %d", setpoint)

        # setpoint = msgdata[0]
        return module_id.to_bytes(1, 'big') + b'\x02\x02' + setpoint.to_bytes(1, 'big')

    if mtype == msgtype.set_angular_velocity_setpoint:
        setpoint_float = msgdata[0] #comes in range -1 to 1, 0 means stop
        setpoint = int(setpoint_float * 127 + 128)

        # TODO RESCALE setpoint
        return module_id.to_bytes(1, 'big') + b'\x02\x03' + setpoint.to_bytes(1, 'big')

    if mtype == msgtype.set_torque_setpoint:
        setpoint_float = msgdata[0] #comes in range -1 to 1, 0 means stop
        setpoint = int(setpoint_float * 127 + 128)

        # TODO RESCALE setpoint
        return module_id.to_bytes(1, 'big') + b'\x02\x04' + setpoint.to_bytes(1, 'big')

    if mtype == msgtype.set_PID_constants:
        controller_type = msgdata[0]
        P = msgdata[1]
        I = msgdata[2]
        D = msgdata[3]

        return module_id.to_bytes(1, 'big') + b'\x0D\x0A' + controller_type.to_bytes(1, 'big') + P.to_bytes(4, 'big') + I.to_bytes(4, 'big') + D.to_bytes(4, 'big')

    if mtype == msgtype.disable_motors:
        return b'\xFF\x01\xFE'
        # return b'x02\x01\xFE'

    if mtype == msgtype.initialize_addresses:
        return b'\xff\x02\xff\x00'

    return None

def get_num_expected_answer_bytes(mtype, msgdata):
    header_bytes = 2
    ack_bytes = 4
    # request_id = bytearr[3]

    # read encoder angles request
    if mtype == msgtype.initialize_addresses:
        return header_bytes + 2
    if mtype == msgtype.read_encoder_angles:
        return header_bytes + 4

    # read current request
    if mtype == msgtype.read_current:
        return header_bytes + 3

    # set PID constants (gets ack)
    if mtype == msgtype.set_PID_constants:
        return header_bytes + 2

    if mtype == msgtype.disable_motors:
        return 3

    if mtype == msgtype.set_PID_constants:
        return ack_bytes

    return 0

def msg_to_str_for_humans(mtype, msgsender, msgval):
    valuesstring = ""
    if mtype == mtype.set_angle_setpoint:
        valuesstring = str(msgval[0]) + "[deg], "
    if mtype == mtype.read_encoder_angles:
        valuesstring = str(msgval[0]) + "[deg], " + str(msgval[1]) + "[deg]"
    if mtype == mtype.read_current:
        valuesstring = str(msgval[0]) + " [A]"
    if mtype == mtype.initialize_addresses:
        valuesstring = str(msgval[-1]) + " modules"
    return "TYPE{" + msgdict[mtype] + "} SENDER{" + str(msgsender) + "} MSGVAL{" + valuesstring + "}"


# # #}
