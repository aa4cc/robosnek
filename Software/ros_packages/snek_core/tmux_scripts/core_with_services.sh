#!/bin/bash

session="ROBOSNEK"

tmux new-session -d -s $session

window=0
tmux rename-window -t $session:$window 'CORE'
tmux send-keys -t $session:$window 'roslaunch snek_core snek_core.launch' C-m

window=1
tmux new-window -t $session:$window -n 'SETPOINT SRV'
tmux send-keys -t $session:$window 'rosservice call /set_module_setpoint "module_address: 1
control_type: 0
setpoint: 90.0"'

window=2
tmux new-window -t $session:$window -n 'ESTOP SRV'
tmux send-keys -t $session:$window 'rosservice call /emergency_stop'

window=3
tmux new-window -t $session:$window -n 'KEYBOARD CONTROLLER'
tmux send-keys -t $session:$window 'waitForRos; roslaunch snek_keyboard_control keyboard_control.launch'

window=3
tmux new-window -t $session:$window -n 'KEYBOARD CONTROLLER'
tmux send-keys -t $session:$window 'waitForRos; roslaunch snek_keyboard_control keyboard_control.launch'

# window=3
# tmux new-window -t $session:$window -n 'evaluation'
# tmux send-keys -t $session:$window 'waitForRos; roslaunch aro_control evaluation.launch' C-m

tmux attach-session -t $session
