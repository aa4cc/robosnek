import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.isOpen()

#INIT
ser.write(b'\xff\x02\xff\x00')
out = ser.read(4)
print("OUT FROM INIT")
print(out)
# ser.close()


#SET SETPOINT OF WHEEL

#SET SETPOINT OF BENDY MODULE
# minangle = 40
# maxangle = 180
# plaintext_num = minangle
# # ser.write(b'\x02\x02\x02\x8C')
# bumps = 0
# dir = 1
# increment = 10
# while bumps  < 10:
#     plaintext_num += dir * increment
#     ser.write(b'\x02\x02\x02' + plaintext_num.to_bytes(1, 'big'))

#     if dir > 0 and plaintext_num > maxangle:
#         plaintext_num = maxangle
#         dir = -1
#         bumps +=1
#         print("BUMP")

#     if dir < 0 and plaintext_num < minangle:
#         plaintext_num = minangle
#         dir = 1
#         bumps +=1
#         print("BUMP")

#     time.sleep(0.1)
#     # print('sent')


# READ ENCODER DATA
# for i in range(100):
#     ser.write(b'\x02\x01\x00')
#     out = ser.read(6)
#     print("OUT:")
#     print(out)
#     # print(out[-1])
#     # print(out[-2])
#     # print(out[-3])
#     # intval1 = out[-2].from_bytes(1, 'big')
#     # intval2 = out[-1].from_bytes(1, 'big')
#     intval1 = out[-2]
#     intval2 = out[-1]
#     print("enc1: ", intval1, " enc2: ", intval2)
#     time.sleep(0.1)

# READ CURRENT
# for i in range(100):
#     ser.write(b'\x02\x01\x01')
#     out = ser.read(5)
#     print("OUT:")
#     print(out)
#     # print(out[-1])
#     # print(out[-2])
#     # print(out[-3])
#     # intval1 = out[-2].from_bytes(1, 'big')
#     # intval2 = out[-1].from_bytes(1, 'big')
#     intval1 = out[-1]
#     print("current: ", intval1)
#     time.sleep(0.1)


# READ TORQUE
print("CALIBRATING TORQUE")
# time.sleep(0.2)
ser.write(b'\x02\x01\x00')
print("WRITTEN COMMAND")
out = ser.read(6)
print("OUT:")
print(out)
# print(out[-1])
# print(out[-2])
# print(out[-3])
# intval1 = out[-2].from_bytes(1, 'big')
# intval2 = out[-1].from_bytes(1, 'big')
intval1 = out[-2]
intval2 = out[-1]

calibration_val = intval1 + intval2
time.sleep(0.1)

# for i in range(500):
#     ser.write(b'\x02\x01\x00')
#     out = ser.read(6)
#     print("OUT:")
#     print(out)
#     # print(out[-1])
#     # print(out[-2])
#     # print(out[-3])
#     # intval1 = out[-2].from_bytes(1, 'big')
#     # intval2 = out[-1].from_bytes(1, 'big')
#     intval1 = out[-2]
#     intval2 = out[-1]
#     # print("enc1: ", intval1, " enc2: ", intval2)
#     print("dif: ", intval1 + intval2 - calibration_val)
#     # time.sleep(0.1)

# ser.close()
# print("CHANGED")



#SET SETPOINT OF BENDY MODULE
minangle = 40
maxangle = 180
plaintext_num = minangle
# ser.write(b'\x02\x02\x02\x8C')
bumps = 0
dir = 1
increment = 5
while bumps  < 5:
    # WRITE
    plaintext_num += dir * increment
    ser.write(b'\x02\x02\x02' + plaintext_num.to_bytes(1, 'big'))

    if dir > 0 and plaintext_num > maxangle:
        plaintext_num = maxangle
        dir = -1
        bumps +=1
        print("BUMP")

    if dir < 0 and plaintext_num < minangle:
        plaintext_num = minangle
        dir = 1
        bumps +=1
        print("BUMP")

    #READ 
    ser.write(b'\x02\x01\x00')
    out = ser.read(6)
    intval1 = out[-2]
    intval2 = out[-1]
    print("dif: ", intval1 + intval2 - calibration_val)
    # print("enc1: ", intval1, " enc2: ", intval2)

    time.sleep(0.05)
    # print('sent')






# print 'Enter your commands below.\r\nInsert "exit" to leave the application.'

# input=1
# while 1 :
#     # get keyboard input
#     input = raw_input(">> ")
#         # Python 3 users
#         # input = input(">> ")
#     if input == 'exit':
#         ser.close()
#         exit()
#     else:
#         # send the character to the device
#         # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
#         ser.write(input + '\r\n')
#         out = ''
#         # let's wait one second before reading output (let's give device time to answer)
#         time.sleep(1)
#         while ser.inWaiting() > 0:
#             out += ser.read(1)
            
#         if out != '':
#             print ">>" + out
