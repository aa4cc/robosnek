import time
import serial
import rospy
from snek_core.common_utils import *
from snek_msgs.srv import *
from std_srvs.srv import Trigger
# from std_msgs.msg import Int32MultiArray, MultiArrayLayout, MultiArrayDimension
# import std_srvs.srv


class SnekCore(object):

# # #{ INIT
    def __init__(self):
        self.is_initialized = False
        self.continuous_query_enabled = rospy.get_param('~continuous_query_enabled', 'map')
        self.control_freq = rospy.get_param('~control_freq', 10.0) # control loop frequency (Hz)
        self.body_config = rospy.get_param('~body_config', [])

        # self.cmd_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1) # Publisher of velocity commands
        # self.lookahead_point_pub = rospy.Publisher('/vis/lookahead_point', Marker, queue_size=1) # Publisher of the lookahead point (visualization only)
        # self.path_pub = rospy.Publisher('/vis/path', Path, queue_size=2)
        # self.waypoints_pub = rospy.Publisher('/vis/waypoints', PointCloud, queue_size=2)

        # self.tf = tf2_ros.Buffer()
        # self.tf_sub = tf2_ros.TransformListener(self.tf)

        # SERIAL INIT
        rospy.loginfo('---- Initializing serial port ----')
        self.serial_port_str = rospy.get_param('~serial_port_str', '/dev/ttyS0')
        try:
            # Init of serial
            self.ser = serial.Serial(
                # port='/dev/ttyS0',
                port=serial_port_str,
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=2
            )
            self.ser.isOpen()
        except Exception as e:
            rospy.logerr('---- Serial port not open, initialization failed ----')
            print(e)

        # MODULE INIT
        rospy.loginfo('---- Initializing modules ----')
        resp_type, resp_sender, resp_val = self.send_msg_to_robot(msgtype.initialize_addresses)
        if resp_type == msgtype.invalid_message or resp_val[0] == 0:
            rospy.logerr('---- Initialization failed ----')
            return
        rospy.loginfo('----Snek core initialized with %d modules ----', resp_val[0])


        # SERVICE INIT
        s1 = rospy.Service('read_module_data', ReadModuleData, self.handle_ReadModuleData_srv)
        s2 = rospy.Service('set_module_setpoint', SetModuleSetpoint, self.handle_SetModuleSetpoint_srv)
        s3 = rospy.Service('set_module_pid_constants', SetModulePIDConstants, self.handle_SetPIDConstants_srv)
        s4 = rospy.Service('emergency_stop', Trigger , self.handle_EmergencyStop_srv)

        # LISTENER INIT
        self.commands_subscriber = rospy.Subscriber('module_setpoints', ModuleSetpointArray, self.handle_ModuleSetpointArrayMsg)

        # INIT BODY CONFIG PUBLISHER 
        self.pub_body_config = rospy.Publisher('body_config', BodyConfigMsg, queue_size=20) # Publisher of velocity commands
        self.timer_body_config = rospy.Timer(rospy.Duration(1.0), self.publishBodyConfig)

        # HACK
        #rp = rospy.Rate(10)
        #while True:
        #    # self.send_msg_to_robot(msgtype.read_encoder_angles, 1)
        #    # time.sleep(0.1)
        #    rp.sleep()


        self.is_initialized = True
# # #}

# # #{ MSGS CALLBACKS
    def publishBodyConfig(self, timerevent):
        msg = BodyConfigMsg()
        # msg.data = []
        for m in self.body_config:
            for i in m:
                msg.data.append(i)
        # print("publishing body config: ", msg)
        self.pub_body_config.publish(msg)

    def handle_ModuleSetpointArrayMsg(self, msg_arr):
        rospy.loginfo("Array of setpoint msgs of len %d received", len(msg_arr.msgs))

        if not self.is_initialized:
            rospy.logwarn("But robot is not initialized!")
            return

        #TODO check if msgs are valid
        for msg in msg_arr.msgs:
            mtype = None
            if msg.control_type == 0:
                mtype = msgtype.set_angle_setpoint
            elif msg.control_type == 1:
                mtype = msgtype.set_angular_velocity_setpoint
            elif msg.control_type == 2:
                mtype = msgtype.set_torque_setpoint

            self.send_msg_to_robot(mtype, msg.module_address, [msg.setpoint], True) #MIN

        rospy.loginfo("All %d msgs sent through serial to robot", len(msg_arr.msgs))
# # #}

# # #{ SERVICE CALLBACKS
    def handle_SetModuleSetpoint_srv(self, req):
        rospy.loginfo("[SRV REQUEST] SET SETPOINT")

        resp = SetModuleSetpointResponse()
        if req.control_type == 0:
            self.send_msg_to_robot(msgtype.set_angle_setpoint, req.module_address, [req.setpoint], True)
            resp.was_successful = True
            return resp

    def handle_ReadModuleData_srv(self, req):
        rospy.loginfo("[SRV REQUEST] READ MODULE DATA")

        resp = ReadModuleDataResponse()
        if req.data_type == 1:
            resp_type, resp_sender, resp_val = self.send_msg_to_robot(msgtype.read_encoder_angles, req.module_address)
            if resp_type == msgtype.invalid_message:
                resp.was_successful = False
                return req
            resp.was_successful = True
            resp.response_data = resp_val
            return resp

    def handle_SetPIDConstants_srv(self, req):
        rospy.loginfo("[SRV REQUEST] SET PID CONSTANTS")
        #TODO
        pass

    def handle_EmergencyStop_srv(self, req):
        rospy.loginfo("SRV REQUEST: EMERGENCY STOP")
        self.send_msg_to_robot(msgtype.disable_motors)
        rospy.loginfo("ROBOT MOTORS DISABLED")
        resp = {}
        resp['success'] = True
        resp['message'] = "motors disabled"
        return resp
# # #}

# # #{ SEND_MSG_TO_ROBOT
    def send_msg_to_robot(self, msg_type, module_address = None, msg_data = None, verbose = False):
        requestmsg = construct_serial_msg(msg_type, module_address, msg_data)
        # print("SENDING MSG TO ROBOT: ", requestmsg)
        num_resp_bytes = get_num_expected_answer_bytes(msg_type, msg_data)
        rospy.loginfo("SENDING MSG TO ROBOT: " + bytearr_to_intstr(requestmsg) + " ... EXPECTED NUM RESPONSE BYTES: " + str(num_resp_bytes))
        if verbose:
            rospy.loginfo("SENDING MSG: %s", msg_to_str_for_humans(msg_type, module_address, msg_data))

        # print(requestmsg)
        self.ser.write(requestmsg)

        # TODO handle timeout
        if num_resp_bytes == 0:
            return True

        # TIMEOUT HERE
        response = self.ser.read(num_resp_bytes)
        # print("READ: ", bytearr_to_intstr(response), " = ", response)
        rospy.loginfo("READ: %s", bytearr_to_intstr(response))
        if num_resp_bytes != len(response):
            rospy.logerr("READ %d / %d BYTES! TODO - TIMEOUT", len(response), num_resp_bytes)
            return msgtype.invalid_message, 0, [0]


        resp_type, resp_sender, resp_val = parse_serial_msg_data(response, msg_type)
        rospy.loginfo("RESPONSE DECODED: %s", msg_to_str_for_humans(resp_type, resp_sender, resp_val))

        return resp_type, resp_sender, resp_val
# # #}

# # #{ READ_ALL_STATES_AND_PUBLISH
    def read_all_states_and_publish(self):
        num_modules = len(self.body_config)
        for module_idx in range(num_modules):
            module = self.body_config[module_idx]
            module_address = module_idx + 1

            if module[0] == moduletype.bendy_module_1axis:
                resp_type, resp_sender, resp_val = construct_serial_msg(msgtype.read_current, module_address, [])

            elif module[0] == moduletype.wheel_module:
                resp_type, resp_sender, resp_val = construct_serial_msg(msgtype.read_current, module_address, [])
                # resp_type, resp_sender, resp_val = self.send_msg_to_robot(msgtype.read_current, 

                # SERIAL!!!
                pass

            else:
                rospy.logwarn('Unrecognized module type in body config!!')
# # #}


if __name__ == '__main__':
    rospy.init_node('snek_core', log_level=rospy.INFO)
    node = SnekCore()
    rospy.spin()


