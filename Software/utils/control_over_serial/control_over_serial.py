import numpy as np
import serial as S
import struct
import argparse

COMMAND_WRITE = 1
COMMAND_READ = 2

def com_with_board(ser):
    inp = input("Enter your command\n")
    inp2 = None
    if inp == 's':
        inp2 = float(input("Enter new setpoint\n"))
        ser.write(COMMAND_WRITE.to_bytes(1, 'big'))
        ser.write(bytearray(struct.pack("f", inp2)))
    elif inp == 'r':
        ser.write(COMMAND_READ.to_bytes(1, 'big'))
        pos = bytearray(4)
        ser.readinto(pos)
        pos = struct.unpack('f', pos)
        print(f"just read {pos[0]}")
    elif inp == 'q':
        return False

    return True




def main(args):
    print(f"opening {args.port} with baudrate {args.baudrate}, {args.stopbits} stopbits and {args.parity} parity")
    ser = S.Serial(args.port)
    ser.baudrate = args.baudrate
    if args.stopbits is not None and args.stopbits <= 2:
        ser.stopbits = args.stopbits
    elif args.stopbits is not None:
        print("Invalid number of stopbits, max is 2")
        return
    else:
        print("Invalid setting of stopbits!")
        return
    if args.parity is None or args.parity == 'odd' or args.parity == 'even':
        if args.parity == 'odd':
            ser.parity = S.PARITY_ODD
        if args.parity == 'even':
            ser.parity = S.PARITY_EVEN
    else:
        print("Invalid setting of parity!")
        return
    ser.timeout = args.timeout
    if ser.is_open:
        print("Serial is opened")
    else:
        print("Something went wrong, serial is not opened")
        return
    #ser.open()

    while com_with_board(ser):
        pass

    ser.close()
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--port')
    parser.add_argument('--baudrate', type=int)
    parser.add_argument('--stopbits', type=int)
    parser.add_argument('--parity')
    parser.add_argument('--timeout', type=float)
    args = parser.parse_args()

    main(args)
