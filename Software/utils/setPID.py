import serial as S
import struct


def main():
    ser = S.Serial('COM12', 115200, parity=S.PARITY_NONE, stopbits=S.STOPBITS_ONE, timeout=5.)
    print(ser.read(6))
    ser.write(b'\xff\x02\xff\x00')
    print(ser.read(4))
    PID_no = b'\x02'
    P = 100.
    I = 0.00
    D = 40.
    ser.write(b'\x01\x0E\x0A' + PID_no + bytearray(struct.pack("f", P)) + bytearray(struct.pack("f", I)) + bytearray(struct.pack("f", D)))
    print(ser.read(4))

if __name__ == '__main__':
    main()
