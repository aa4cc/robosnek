![Alt text](media/snek_over_prague.jpg?raw=true "Title")
# Introduction
Hi! RoboSnek is an extremely low-cost modular robotic snake system with custom torque-control capable servos. We developed this project over the course of the 2022 summer semester and here you can hopefully learn how to re-create or use this system. The only existing instance of the robot as of February 2023 is in the aa4c lab at Karlovo Náměstí, but the robot is very cheap to build if you want your own, and you can choose how many of each module you build thanks to its modular nature!

You can find a presentation of the project <b><a href="https://docs.google.com/presentation/d/e/2PACX-1vR7Y2aVMcrMgu64HutL4KcoOmWKw6Ym_JRJuvPrOUr-_A0P30t1J-1SthWG5_IejlBaikuK6vdRx3ck/pub?start=false&loop=false&delayms=3000&slide=id.g12d614dd30b_0_126">here</a></b>

# Module description
## Head module
This module simply consists only of a Raspberry pi (currently 3B+), a powerbank and a 3D printed housing for it. In the future, an IMU could be fitted into the housing as well. Additionally, it would be better if the powerbank was replaced by a voltage regulator for supplying the Raspberry Pi from the main LiPo battery, since powerbanks usually have almost too low amperage for the computer, and it would reduce the weight of the module.
## Battery module
This module is also very simple and contains only a 3D printed housing for a 3S (11.1V) battery and a simple power distribution board (not in the HW files, but it is just a fuse and a switch for the battery and branches into 2 pairs of 14AWG cables). There is no Raspberry Pico for forwarding messages, so the TX and RX cables just go through the module.
## Servo module
In principle, it is a series elastic actuator (SEA) with angle and torque control. Consists of a Raspberry Pico, an H-bridge for motor control (we recommend the non-cheapest ones, since the ones we had for under 1$ were overheating and shutting down the motors all the time), an inexpensive JGA25-370 (35 RPM, 12V) motor, 2 AS5600 magnetic encoders and an electronics board (hand-soldered, we did not have time to design a mass-producable PCB). The entire electronics diagram can be found in the Hardware folder. All of the mechanical parts are 3D-printed and take about ~10hrs of cumulative print time on a Prusa Mk3s.

The main feature of the servo module is that it can sense and control either torque or total angle. This is done by using the 2 magnetic encoders and connecting the shaft of the motor with the second rigid part by a radial spring (3D printed). One encoder measures the angle between the 2 rigid parts. The second one measures the angle between the first part and the shaft of the motor. Since there is a spring between the shaft and the second part, we can subtract these angles and get the deflection of the springs. We did not have time to measure the relationship of spring deflection vs output torque, so we only control deflection currently, but that can be easily fixed by taking a newtonmeter and creating a lookup table of torque corresponding to deflection. There is also a measurement resistor and current reading is implemented, an it also nicely corresponds with the deflection.

The main problem of the servo at the moment is that the spring component is fully 3D-printed out of PETG. We have tried over 40 spring designs, but the spring always breaks after a few hours of use (or faster under heavy impacts). The robot would greatly benefit from a better, most likely non-monolithic spring design (e.g. using thin pieces of metal as spokes).
## Wheel module
This module's electronics are the same as for the servo module, without the encoders (simply due to lack of time to implement them) and with a faster variant of the JGA25-370 motor (60RPM). Since it does not feature any sensors, this module only has feedforward PWM control of the motor. The majority of it is 3D printed except for a metal axis which connects the motor output to the wheels. This metal axis is quite heavy so this module could be improved by redesigning it for a lower weight and perhaps adding encoders so that the motors can be angular-velocity controlled.
# General properties
- The electronics design for the modules can be found in the Hardware folder.
- The software for the Raspberry Picos in the servo and wheel modules are in the Software folder.
- Most of the mechanical parts were 3D printed with the PETG filament type, with varying infill (mostly default, but the stressed parts like the spring or anything mounted to an axis had 80% or 100% infill).
- Connecting the modules is quite simple, there are 4 wires on each end of all modules (except the head) - 2 14AWG cables for power and 2 thinner wires for TX and RX of UART communication. This way, the modules can be connected in any orientation to one another.
- The orientation of the modules is the one thing about the configuraiton that must be entered by the user into a ROS config file. The details are in the snek_core package. This could be automated by adding an accelerometer to each module, but we skipped that for simplicity's sake.
# Communication
For communication between modules, we developed our own protocol, in detail described at <b>https://docs.google.com/spreadsheets/d/e/2PACX-1vQtCfD3XgWizi6efS6hVYXc300y-V3s7uCaTiTnN9mgG98plwXu_kUwUnIru0USVP-XFyTsHdfiSsXP/pubhtml</b>. The communication is master-slave, with the Raspberry Pi sending UART messages requesting action or data from the modules. 

Each module has an address, assigned at startup of the snek_core core.py node, the address simply starts at 0=master and increases with distance from the master. During communication, the modules (after init) know which way is the master, and so they know if a message is coming from the master direction. Each module, if it is getting a message from master direction, looks at the module address, and if it isnt its own, it forwards it to the next module. If the module address and target address match, the module either does an action, or it sends some requested data back to master (and all modules simply forward this message in the direction of master).

The last module has the UART pins shorted (TX to RX, RX to TX) so the last module knows that it is the last module if it reads the same message that it sends out in the direction away from master. 
The drawback of this approach is that it does not allow modules to be added or disabled dynamically, but that can be the focus of future work.
# ROS 
The main computer is running ROS Noetic and all the high-level software was made as ROS nodes. The ROS packages are in the Software/ros_packages folder. Installing the software on a fresh Raspberry Pi is quite straightforward, you only need to load the Ubuntu Server image onto the Pi SD card (best using the Pi Imager software), install ROS, create a workspace, add the ros_packages packages into the source folder and build.
- ros_core - the core.py node serves as the bridge between ROS and the hardware - it takes care of module initialization and communication, and listens to ROS messages requesting data reading, emergency stop or setpoints for the wheel/servo module
- snek_keyboard_control - offers a simple script that read keyboard input (WASD) and drives the configuration on the intro image as a car with the JK keys lifting or prolapsing the robot's backside (the vertically-oriented servo). This script could be easily adapted for other configurations. We usually ran this script on our own PC.
- snek_visualizer - contains URDF files of individual modules, a script for building the URDF of the entire robot for any given configuration and importantly, it also contains the Reinforcement Learning environment and training scripts. Also the snek_tf_publisher.py publishes rviz markers that visualize the state of the robot.
- snek_msgs - contains the necessary custom messages for ROS communication of the RoboSnek
# Reinforcement Learning Environment for Stair Climbing
![Alt text](media/sim_snek.png?raw=true "Title")

For the project, we also built a PyBullet AI Gym environment with 2 world models made in Blender (rough terrain and stairs) complete with a script that trains the snek (in any given module configuration) to go forward (in the simulation, Snek is given the orientation of the head as input, which could be achieved in real world with an IMU + possibly a compass or visual odometry) and even climb stairs.

The pybullet AI gym environment can be found in Sowftware/ros_packages/snek_visualizer/scripts. There is a readme in there describing how to run the simulation, train agents on stair climbing and then loading them. The whole thing is not perfect (the weights and torques were just guessed and there seems to be some weirdness between learning and testing (PPO seems to add some noise in control during traing, without which it doesnt really work well in testing)) but can serve as a base for any RL project with RoboSnek.

A video of the RoboSnek learning to climb stairs in simulation can be found here: <b>https://www.youtube.com/watch?v=A939OQfjrN8</b>
# WHY/FAQ
- Why did we make it so low-cost? - so that anyone can build it! The motors are not really strong (one servo module can hold one wheel module reliably, but not much more), but it is quite sufficient for crawling and possibly for stairs/obstacle climbing. Furthermore, it is meant as a research/education platform so longevity of the parts was also not really the target. If anyone wants, the robot could be remade with sturdier materials but here we tried to make a very simple, very affordable robot for everyone.
- Why torque sensing and control? - The main hypothesis is that ALL animals somehow sense forces during locomotion, and so to allow climbing over obstacles/getting unstuck in rugged terrain, the robot should at least sense torques in its joints (ideally having some kind of skin as well). This should hopefully help reinforcement learning of locomotion on varying terrains a lot, but we did not have time to test it formally. 
- How much does the robot cost in parts? And how long to assemble? - The configuration with 2 servo and 2 wheel modules, head and battery cost around 3800 CZK = 180 USD. Assembly time of the servo and wheel module (once everything is 3D printed) is about 5hrs, out of which 4hrs are for assembling the electronics (would be much faster if we had a circuit board assembled industrially).
# Future work possibilities
- Taking the locomotion patterns learned in the reinforcement learning simulation into the real world (would mean doing the work to bridge the reality gap = adding the IMU, better modeling the robot weights, moments of inertia, servo dynamics, ...)
- Adding a camera to the head module - would allow experimenting with visual odometry, possibly SLAM (e.g. LSD-SLAM, ORB-SLAM, VINS-Mono, RATSLAM, ...), estimating depth from camera (for more informed obstacle climbing) and many other wonderful things.
- Adding an IMU to the head module (likely neccessary for any reasonable autonomous movement).
- Designing robust non-monolithic spring elements for the servo, so that they can last longer (current ones break after a few hrs of use, sometimes less)
- Designing a lighter wheel module (current one has a steel axis in it)
- Designing a circuit board for the wheel and servo modules that can be produced industrially at scale so people don't have to hand-solder everything (currently it takes around 4hrs to prepare electronics for 1 module).

